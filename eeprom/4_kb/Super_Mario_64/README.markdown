# EEPROM Editor for _Super Mario 64_
_Super Mario 64_ stores game configuration and progress on an EEPROM chip. EEPROM is a type of non-volatile memory preserved when the Nintendo 64 is not running, allowing the player to resume the game any time. This manual gives documentation on how to use the program and how the configuration bits are encoded in the EEPROM.
***

##Using the EEPROM Editor

>Editor is not yet complete.

##How _Super Mario 64_ Controls the EEPROM
The type of EEPROM used is four-kilobit, so there are 512 byte addresses. Although most progress is encoded on a bit-precision level, the bits are generally all grouped into bytes representing a shared category.

All that is essential for the save editor is in the range of byte addresses `0x0000:0x0037`.

Flags that are **unused** are literally not used by the game's write-access to the EEPROM, because no legitimate actions in the game will create these values. It is still possible that setting them in the EEPROM will have an effect on the game. Flags that are **reserved** are not only unused but make no difference when read in, too. Programmers are free to implement a new and customized use for them.

### Mario cap ghost string (`0x0000:0x0007`)
At any time, if Mario has lost his cap, saving the game will record this state, the world the cap was lost in, and 3-D coordinate information of where it was lost. These bytes are not read anymore; position within Snowman's Land is randomized. It was likely a debugging process for bytes whose purpose was revised throughout development, and the game will never set them back to zero.

### Stars in Mushroom Castle (`0x0008`)
There are fifteen secret stars in the game, but only five of them appear in the castle itself.

* `0000 0001`: Met Toad outside Hazy Maze Cave.
* `0000 0010`: Met Toad on the second floor.
* `0000 0100`: Met Toad on the third floor.
* `0000 1000`: Caught the rabbit in the basement.
* `0001 0000`: Caught the rabbit again.
* `0010 0000`: unused (identity of beta star)
* `0100 0000`: unused (identity of beta star)
* `1000 0000`: reserved

### Where is Mario's cap? (`0x0009`)
The strict specifier for where Mario's cap was left.  Its purpose is mixed with a separate boolean flag from the next byte's category, which had no more space left over. The following boolean flags are usable to configure.

* `0000 0001`: Snowman's Land
* `0000 0010`: Shifting Sand Land
* `0000 0100`: Tall, Tall Mountain
* `0000 1000`: reserved
* `0001 0000`: Door to third floor (will need fifty stars to open)
* `0010 0000`: reserved
* `0100 0000`: reserved
* `1000 0000`: reserved

### Uncovered access links throughout Mushroom Castle (`0x000A`)
The game has to remember if animations for opening star doors have already been seen to prevent them from replaying.

* `0000 0001`: Opening to Bowser in the Fire Sea freed
* `0000 0010`: Mushroom Castle moat drained
* `0000 0100`: Door to the Princess' secret slide (will need one star to open)
* `0000 1000`: Door to Whomp's Fortress (will need one star to open)
* `0001 0000`: Door to Cool, Cool Mountain (will need three stars to open)
* `0010 0000`: Door to Jolly Roger Bay (will need three stars to open)
* `0100 0000`: Door to Bowser in the Dark World (will need eight stars to open)
* `1000 0000`: Door to Dire, Dire Docks (will need thirty stars to open)

### Generic progress flags (`0x000B`)
The following boolean flags are usable to configure.

* `0000 0001`: Game slot occupied in File Select
* `0000 0010`: Wing cap switch on
* `0000 0100`: Metal cap switch on
* `0000 1000`: Invisibility cap switch on
* `0001 0000`: Have key to basement
* `0010 0000`: Have key to second floor
* `0100 0000`: Door to basement unlocked
* `1000 0000`: Door to second floor unlocked

### Star Reference String (`0x000C:0x0023`)
The following byte addresses are all part of a string of bytes whose low-order seven bits correspond to seven stars in each of the fifteen worlds beyond the paintings. Not all seven flags correspond to stars that actually exist ("unused"), but the game will still scan in the number of stars from this reference. The eighth, high-order flag is either reserved or used for the previous level's cannon.

* `0x000C` Bob-omb Battlefield
* `0x000D` Whomp's Fortress (Bob-omb Battlefield cannon flag)
* `0x000E` Jolly Roger Bay (Whomp's Fortress cannon flag)
* `0x000F` Cool, Cool Mountain (Jolly Roger Bay cannon flag)
* `0x0010` Big Boo's Haunt (Cool, Cool Mountain cannon flag)
* `0x0011` Hazy Maze Cave
* `0x0012` Lethal Lava Land
* `0x0013` Shifting Sand Land
* `0x0014` Dire, Dire Docks (Shifting Sand Land cannon flag)
* `0x0015` Snowman's Land
* `0x0016` Wet-Dry World (Snowman's Land cannon flag)
* `0x0017` Tall, Tall Mountain (Wet-Dry World cannon flag)
* `0x0018` Tiny-Huge Island (Tall, Tall Mountain cannon flag)
* `0x0019` Tick Tock Clock (Tiny-Huge Island cannon flag)
* `0x001A` Rainbow Ride
* `0x001B` Bowser in the Dark World (Rainbow Ride cannon flag)
* `0x001C` Bowser in the Fire Sea
* `0x001D` Bowser in the Sky
* `0x001E` The Princess' Secret Slide
* `0x001F` Cavern of the Metal Cap
* `0x0020` Tower of the Wing Cap
* `0x0021` Vanish Cap Under the Moat
* `0x0022` Wing Mario Over the Rainbow
* `0x0023` The Secret Aquarium (Wing Mario Over the Rainbow cannon flag)
* `0x0024`: unused

### High-Scores for Coins (`0x0025:0x0033`)
The array of fifteen unsigned characters storing the highest coin score obtained. The array elements correspond to the fifteen worlds beyond the paintings, in their order from the above list.

### EEPROM Magic Number (`0x0034:0x0035`)
The short integer who sixteen bits must model the Japanese syllable "DA". Many people refer to this constant, of its context, as a magic number. It is fixed to this value, as the loaded and write-back EEPROM memory in the game is empty and void of the EEPROM data if the value is anything else. The game uses the $a2 register in an ADDIU instruction to write this immediate back to the EEPROM.

### Additive Checksum (`0x0036:0x0037`)
The unsigned integer whose correct value is the sum of the previous fifty-four bytes. (For the Mario A save slot, this is the sum of all octets in the range of byte-precision addresses `0x0000:0x0035`.) Because 54 * 256 is less than 256 * 256, no integer overflow exceptions are possible. If the game cannot confirm the validity of this measure, it will attempt to transfer file information from the successive fifty-six bytes. (For the Mario A save slot, this is the range of byte-precision addresses `0x0038:0x006F`.)

### Save Slots for Mario B, Mario C, and Mario D (`0x0070:0x01BF`)
All game save slots follow precisely the format explained throughout the range of byte-precision addresses `0x0000:0x006F`.

### EEPROM Parity Octaword (`0x01C0:0x01CF`)
The pair of four doublewords corresponding to immediate writes to data for the game file slots. It is more of a debugger for the game on the order in file erasure or immediate updates.

### Sound Output (`0x01D0:0x01D1`)

 * `0000 0000 0000 0000`: Stereo
 * `0000 0000 0000 0001`: Mono
 * `0000 0000 0000 0010`: Headset

### Language (`0x01D2:0x01D3`)
This word is unused by the NTSC releases. Its use was reserved for the PAL release as a language indicator.

 * `0000 0000 0000 0000`: English
 * `0000 0000 0000 0001`: Fran�ais
 * `0000 0000 0000 0010`: Deutsch

### Reserved Space (`0x01D4:0x01DB`)

### EEPROM Magic Number (`0x01DC:0x01DD`)
There is another magic number used to validiate the EEPROM that is also written back from the $a2 register using the ADDIU CPU instruction. Its ASCII value is the Japanese syllable "HI".

### Additive Checksum (`0x01DE:0x01DF`)
The unsigned integer whose correct value is the sum of the previous thirty bytes. Here, integer overflow exceptions remain impossible. If the game cannot confirm the validity of this measure, it will attempt to transfer file information from the successive thirty-two bytes.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned char EEPROM[0x0200] = "";
unsigned int i = 0x0000; /* register for array index memory addressing */
char string[81] = "";

signed char chr_in(void);
unsigned char str_in(void);
void configure_file(char file);

int main(int argc, char *argv[])
{
    FILE *stream;
    if (argv[1] == NULL)
    {
        printf("Command syntax missing domain.\n");
        return 1;
    }
    if (argc > 2)
    {
        printf("Too many parameters specified.\n");
        return 1;
    }
    stream = fopen(argv[1], "rb");
    if (stream == NULL)
    {
        printf("Specified file was unreadable.\n");
        return 1;
    }
    fread(EEPROM, sizeof(unsigned char), 0x0200, stream);
    fclose(stream);

    while (0 == 0)
    {
        if (string[80])
        {
            break;
        }
        printf("Writing to game slot MARIO_");
        switch (chr_in() & 0xDF)
        {
            case 'A':
                configure_file('A');
                break;
            case 'B':
                configure_file('B');
                break;
            case 'C':
                configure_file('C');
                break;
            case 'D':
                configure_file('D');
                break;
            default :
                printf("Invalid file slot specifier.\n");
                break;
        }
    }

    stream = fopen(argv[1], "wb");
    fwrite(EEPROM, sizeof(unsigned char), 0x0200, stream);
    fclose(stream);
    return 0;
}

void configure_file(char file)
{
    unsigned char data[0x38] = ""; /* MUST be unsigned, prevent (signed char) */
    int cannon = 0;
    for (i = 0x0000; i < 0x0038; ++i)
    {
        data[i] = EEPROM[i + (file - 'A') * 0x0070];
    }

    if (data[0x01] & 0x01)
    {
        printf("Mario's cap was last lost at course %d.\n", data[0x00]);
        printf("Theft coordinates:  x = %d, y = %d, z = %d\n",
               (data[0x02] << 8) + data[0x03],
               (data[0x04] << 8) + data[0x05],
               (data[0x06] << 8) + data[0x07]);
    }
    else
    {
        printf("Mario's cap was never lost before.\n");
    }
    if (data[0x0B] & 0x01)
    {
        unsigned char stars = 0;
        char s = 0, w = 0;
        for (s = 0; s < 7; ++s)
        {
            stars += (data[0x08] >> s) & 0x01;
        }
        for (w = 0x0C; w < 0x25; ++w)
        {
            for (s = 0; s < 7; ++s)
            {
                stars += (data[w] >> s) & 0x01;
            }
        }
        printf("%u stars found in game save.\n\n", stars);
    }
    else
    {
        printf("Game marked as \"NEW\".\n\n");
    }

    printf("0:  Return\n1:  File Signal\n2:  Mario's Cap\n3:  Castle Access\n");
    printf("4:  Star Doors\n5:  Switches\n6:  Castle Stars\n7:  World Stars\n");
    switch (chr_in() & 0x07)
    {
        case 0:
            string[80] = 0xFF;
            unsigned int checksum = 0x0000;
            checksum = data[0x00] + data[0x01] + data[0x02] + data[0x03] +
                       data[0x04] + data[0x05] + data[0x06] + data[0x07] +
                       data[0x08] + data[0x09] + data[0x0A] + data[0x0B] +
                       data[0x0C] + data[0x0D] + data[0x0E] + data[0x0F] +
                       data[0x10] + data[0x11] + data[0x12] + data[0x13] +
                       data[0x14] + data[0x15] + data[0x16] + data[0x17] +
                       data[0x18] + data[0x19] + data[0x1A] + data[0x1B] +
                       data[0x1C] + data[0x1D] + data[0x1E] + data[0x1F] +
                       data[0x20] + data[0x21] + data[0x22] + data[0x23] +
                       data[0x24] + data[0x25] + data[0x26] + data[0x27] +
                       data[0x28] + data[0x29] + data[0x2A] + data[0x2B] +
                       data[0x2C] + data[0x2D] + data[0x2E] + data[0x2F] +
                       data[0x30] + data[0x31] + data[0x32] + data[0x33] +
                       data[0x34] + data[0x35]; /* Loops are for pussies. */
            data[0x36] = checksum >> 8;
            data[0x37] = checksum & 0x00FF;
            break;
        case 1:
            if (EEPROM[0x0B] & 0x01)
            {
                strcpy(string, "occupied");
            }
            else
            {
                strcpy(string, "vacant");
            }
            printf("Game slot is marked as %s.\n", string);
            printf("Mark file as vacant or occupied?  ");
            data[0x0B] = (data[0x0B] & 0xFE) | (chr_in() << 0 & 0x01);
            break;
        case 2:
            switch (data[0x09] & 0x03)
            {
                case 0x00:  strcpy(string, "his head");
                            break;
                case 0x01:  strcpy(string, "Snowman's Land");
                            break;
                case 0x02:  strcpy(string, "Shifting Sand Land");
                            break;
                case 0x03:  strcpy(string, "Tall, Tall Mountain");
                            break;
            }
            printf("Mario's cap is on %s.\n", string);
            printf("Set Mario's cap where?  ");
            data[0x09] = (data[0x09] & 0xF8) | (chr_in() << 0 & 0x07);
            break;
        case 3:
            printf("Repel course 9 entrance?  ");
            data[0x0A] = (data[0x0A] & 0xFE) | (chr_in() << 0 & 0x01);
            printf("Drain castle moat?  ");
            data[0x0A] = (data[0x0A] & 0xFD) | (chr_in() << 1 & 0x02);
            printf("Basement unlocked?  ");
            data[0x0B] = (data[0x0B] & 0xBF) | (chr_in() << 6 & 0x40);
            printf("Have key?  ");
            data[0x0B] = (data[0x0B] & 0xEF) | (chr_in() << 4 & 0x10);
            printf("Second floor unlocked?  ");
            data[0x0B] = (data[0x0B] & 0x7F) | (chr_in() << 7);
            printf("Have key?  ");
            data[0x0B] = (data[0x0B] & 0xDF) | (chr_in() << 5 & 0x20);
            break;
        case 4:
            printf("Star doors have not been implemented yet.\n");
            break;
        case 5:
            printf("Wing cap switch?  ");
            data[0x0B] = (data[0x0B] & 0xFD) | (chr_in() << 1 & 0x02);
            printf("Metal cap switch?  ");
            data[0x0B] = (data[0x0B] & 0xFB) | (chr_in() << 2 & 0x04);
            printf("Invisibility cap switch?  ");
            data[0x0B] = (data[0x0B] & 0xF7) | (chr_in() << 3 & 0x08);
            break;
        case 6:
            printf("Flags set for castle secret stars is %02X.\n", data[0x08]);
            printf("Overwrite flags set with:  ");
            scanf("%x", &data[0x08]);
            break;
        case 7:
            printf("Edit stars for what course number?  ");
            scanf("%u", &i);
            switch (i)
            {
                case  1:  strcpy(string, "Bob-omb Battlefield");
                          cannon = 1;
                          break;
                case  2:  strcpy(string, "Whomp's Fortress");
                          cannon = 1;
                          break;
                case  3:  strcpy(string, "Jolly Roger Bay");
                          cannon = 1;
                          break;
                case  4:  strcpy(string, "Cool, Cool Mountain");
                          cannon = 1;
                          break;
                case  5:  strcpy(string, "Big Boo's Haunt");
                          break;
                case  6:  strcpy(string, "Hazy Maze Cave");
                          break;
                case  7:  strcpy(string, "Lethal Lava Land");
                          break;
                case  8:  strcpy(string, "Shifting Sand Land");
                          cannon = 1;
                          break;
                case  9:  strcpy(string, "Dire, Dire Docks");
                          break;
                case 10:  strcpy(string, "Snowman's Land");
                          cannon = 1;
                          break;
                case 11:  strcpy(string, "Wet-Dry World");
                          cannon = 1;
                          break;
                case 12:  strcpy(string, "Tall, Tall Mountain");
                          cannon = 1;
                          break;
                case 13:  strcpy(string, "Tiny-Huge Island");
                          cannon = 1;
                          break;
                case 14:  strcpy(string, "Tick Tock Clock");
                          break;
                case 15:  strcpy(string, "Rainbow Ride");
                          cannon = 1;
                          break;
                case 16:  strcpy(string, "Bowser in the Dark World");
                          break;
                case 17:  strcpy(string, "Bowser in the Fire Sea");
                          break;
                case 18:  strcpy(string, "Bowser in the Sky");
                          break;
                case 19:  strcpy(string, "The Princess' Secret Slide");
                          break;
                case 20:  strcpy(string, "Cavern of the Metal Cap");
                          break;
                case 21:  strcpy(string, "Tower of the Wing Cap");
                          break;
                case 22:  strcpy(string, "Vanish Cap Under the Moat");
                          break;
                case 23:  strcpy(string, "Wing Mario Over the Rainbow");
                          cannon = 1;
                          break;
                case 24:  strcpy(string, "The Secret Aquarium");
                          break;
                case 25:  strcpy(string, "unused star space");
                          break;
                default:  printf("Invalid world number specifier.\n");
                          return;
            }
            printf("Star flags set:  0x%02X\n", (data[0x0B + i] & 0x7F));
            printf("Overwrite flags set for %s:  \n", string);
            data[0x0B + i] = (data[0x0B + i] & 0x80) | (str_in() & 0x7F);
            printf("High score:  %d coins\n", data[0x24 + i]);
            printf("Specify new high score in coins:  ");
            scanf("%u", &data[0x24 + i]);
            if (cannon)
            {
                printf("Open cannon?  ");
                data[0x0B + i] = (data[0x0B + i] & 0x7F) | (chr_in() << 7);
            }
            break;
        default  :  printf("Invalid write command specifier.\n");
    }

    cannon = (file - 'A') * 0x0070; // re-use cannon register for memory addressing
    for (i = cannon; i < (cannon + 0x0070); ++i)
    {
        EEPROM[i] = data[i - cannon];
    }
    printf("\n");
    return;
}

signed char chr_in(void)
{ /* function to read in ASCII character */
    signed char buffer[2];
    signed int in = 0;

    in = fgetc(stdin);
    buffer[0] = (signed char) in;
    buffer[1] = '\0';

    while (0 == 0)
    {
        if ((in == EOF) || (in == '\n'))
        { /* if stdin FILE pointer ends the current line */
            break;
        }
        in = fgetc(stdin);
    } /* Use fgetc() or getchar() to increment the stdin file pointer. */
    return buffer[0];
}
unsigned char str_in(void)
{ /* function to read in string of binary digits */
    signed char binary[8];
    unsigned char digit = 0; /* reverse endianness of proper digit order */
    signed int in = 0;
    unsigned int sum = 0x0000;
    while (digit < 8)
    {
        in = fgetc(stdin);
        if ((in == EOF) || (in == '\n'))
        { /* if stdin FILE pointer ends the current line */
            break;
        }
        binary[7 - digit] = ((signed char) in) & 0x01;
        ++digit;
    }
    for (digit = 7; digit != 0; --digit)
    {
        sum += binary[7 - digit] << digit; /* fucked up prototype, gonna need to verify-check this later */
    }
    return sum;
}

@ECHO OFF
TITLE MinGW Compiler Suite Invocation
CD C:\MINGW\BIN\

ECHO Invoking CC1.EXE (C compiler)...
GCC.EXE -S -O3 -o ../memory.asm ../memory.c
ECHO.

ECHO Invoking AS.EXE (GNU assembler)...
AS.EXE --statistics -o ../memory.obj ../memory.asm
ECHO.

ECHO Invoking LD.EXE (GNU linker)...
GCC.EXE -s -o ../memory.exe ../memory.obj
PAUSE

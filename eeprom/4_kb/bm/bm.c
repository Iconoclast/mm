#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void decode_property(char property[80]);
static char filepath[64] = "output.bin";

void (*ptr[16])(char string[80]); /* function lookup */

static unsigned char memory[0x0200] = {
/*0000*/'B','A','K','U',002,'H','u',035,000,000,000,000,000,000,000,255,
/*0010*/033,050,046,033,036,053,000,000,062,000,000,000,000,130,130,251,
/*0020*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0030*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0040*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0050*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0060*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0070*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0080*/130,130,000,000,000,000,000,251,000,000,000,000,000,000,000,255,
/*0090*/000,000,000,240,240,240,240,077,000,000,000,000,000,000,000,255,
/*00A0*/033,050,046,033,036,053,000,000,062,000,000,000,000,130,130,251,
/*00B0*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*00C0*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*00D0*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*00E0*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*00F0*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0100*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0110*/130,130,000,000,000,000,000,251,000,000,000,000,000,000,000,255,
/*0120*/000,000,000,240,240,240,240,077,000,000,000,000,000,000,000,255,
/*0130*/033,050,046,033,036,053,000,000,062,000,000,000,000,130,130,251,
/*0140*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0150*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0160*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0170*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0180*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*0190*/000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,255,
/*01A0*/130,130,000,000,000,000,000,251,000,000,000,000,000,000,000,255,
/*01B0*/000,000,000,240,240,240,240,077,000,000,000,000,000,000,000,000,
/*01C0*/000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,
/*01D0*/000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,
/*01E0*/000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,
/*01F0*/000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000 };
int main()
{
    register unsigned char checksum8 = 0x00;
    FILE *stream;

    stream = fopen("bm.ini", "r");
    if (stream == NULL)
    {
        printf("Initialization file not found!\n");
        return 1;
    }
    while (feof(stream) == 0)
    {
        if (fgetc(stream) >> 7 == 1)
        { /* most significant bit of octet enabled? */
            printf("Invalid ASCII mapper resource!\n");
            fclose(stream);
            return 1;
        }
    }
    rewind(stream); /* very similar:  fseek(stream, 0, SEEK_SET); */

    while (feof(stream) == 0)
    {
        register section = 8; /* undefined */
        auto char line[80]; /* MS-/etc. DOS plain text file line size */
        auto char *sections[8] = {"[root", "[dat1", "[dat2", "[dat3"};
        fgets(line, 80, stream);
        switch (line[0])
        {
            case ';':
                printf(line);
                break;
            case '[':
                if (strchr(line, ']') == NULL)
                {
                    printf("Section label minus delimiter.\n");
                    fclose(stream);
                    return 1;
                }
                section = line[4] & 0x03;
                line[5] = '\0';
                if (strcmp(line, sections[section]) != 0)
                {
                    printf("Unknown section was specified.\n");
                    fclose(stream);
                    return 1;
                }
                break;
            default:
                if (strchr(line, '=') == NULL)
                {
                    printf("Junk data found in INI buffer.\n");
                    fclose(stream);
                    return 1;
                }
                decode_property(line);
                break;
        }
    }
    fclose(stream);
    stream = fopen(filepath, "wb");
    fwrite(memory, sizeof(unsigned char), 0x0200, stream);
    fclose(stream);
}

void decode_property(char property[80])
{
    register i = 0;
    register name_len;
    auto char key[77] = ""; /* minus 2 for CRLF, minus 1 for '=' */
    auto char value[77] = ""; /* minus 2 for CRLF, minus 1 for '=' */

    while (property[i] != '=')
    {
        key[i++] = property[i];
    }
    name_len = i;
    ++name_len;
    i = 0;
    while (property[name_len] > 0x1F)
    {
        value[i++] = property[name_len++];
    }
    printf("%s <-- %s\n", key, value);
    return;
}

void set_file_in(char path[80])
{
    FILE *EEPROM;
    EEPROM = fopen(path, "rb");
    if (EEPROM == NULL)
    {
        printf("Warning:  Filepath not loaded.\n");
    }
    else
    {
        fread(memory, sizeof(unsigned char), 0x0200, EEPROM);
    }
    fclose(EEPROM);
    return;
}
void set_file_out(char path[80])
{
    strcpy(filepath, path);
    return;
}

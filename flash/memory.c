/******************************************************************************\
* Project:  128-KB Flash Memory Editor                                         *
* Release:  2012.03.15                                                         *
* License:  none (public domain)                                               *
* Creator:  R. Swedlow                                                         *
\******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#define FLASH_SIZE 0x00020000

unsigned char memory[FLASH_SIZE]; /* buffer for flash data */

signed char shit(signed char error_code);
/* "shit", as in, I wanted to cleverly name the function "abort", until I     *
 * found that the label was reserved by the compiler standard library.  If I  *
 * can't give it a perfect name then I don't really give a shit what          *
 * substitute I choose.  Anyway, it's my custom routine vs. system("PAUSE").  */

int main(int argc, char *argv[])
{
    if (argv[1] == NULL)
    {
        printf("Command syntax missing domain.\n");
        return shit(1);
    }
    if (argc > 4)
    {
        printf("Too many parameters specified.\n");
        return shit(1);
    }
    if (argv[2] == NULL)
    {
        printf("No target address specified.\n");
        return shit(1);
    }
    else
    {
        unsigned long addr = atol(argv[2]); /* might need to use strtoul() */
        FILE *stream;

        stream = fopen(argv[1], "rb");
        if (stream == NULL)
        {
            printf("Specified file was unreadable.\n");
            return shit(1);
        }
        fread(memory, sizeof(unsigned char), FLASH_SIZE, stream);
        fclose(stream);

        if (addr > FLASH_SIZE)
        {
            printf("Flash memory addressing overflow!\n");
            return shit(1);
        }
        else if (atoi(argv[3])) /* only optional command argument */
        {
            addr ^= 0x00000003; /* to adjust address for endian swap */
        }
        scanf("%x", &memory[addr]);
        /* to do:  deprecate scanf() in favor of using command argument */

        stream = fopen(argv[1], "wb");
        fwrite(memory, sizeof(unsigned char), FLASH_SIZE, stream);
        fclose(stream);
    }
    return 0;
}

signed char shit(signed char error_code) /* abort()*, goddammit! */
{
    printf("Press Enter to exit.\n");
    fgetc(stdin); /* Or getchar(), same difference for you high-level chimps. */
    return error_code;
}

	.file	"memory.c"
	.comm	_memory, 131072, 5
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
	.align 4
LC0:
	.ascii "Command syntax missing domain.\0"
	.align 4
LC1:
	.ascii "Too many parameters specified.\0"
LC2:
	.ascii "No target address specified.\0"
LC3:
	.ascii "rb\0"
	.align 4
LC4:
	.ascii "Specified file was unreadable.\0"
	.align 4
LC5:
	.ascii "Flash memory addressing overflow!\0"
LC6:
	.ascii "%x\0"
LC7:
	.ascii "wb\0"
	.text
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB6:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$32, %esp
	call	___main
	movl	12(%ebp), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L2
	movl	$LC0, (%esp)
	call	_puts
	movl	$1, (%esp)
	call	_shit
	movsbl	%al, %eax
	jmp	L3
L2:
	cmpl	$4, 8(%ebp)
	jle	L4
	movl	$LC1, (%esp)
	call	_puts
	movl	$1, (%esp)
	call	_shit
	movsbl	%al, %eax
	jmp	L3
L4:
	movl	12(%ebp), %eax
	addl	$8, %eax
	movl	(%eax), %eax
	testl	%eax, %eax
	jne	L5
	movl	$LC2, (%esp)
	call	_puts
	movl	$1, (%esp)
	call	_shit
	movsbl	%al, %eax
	jmp	L3
L5:
	movl	12(%ebp), %eax
	addl	$8, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_atol
	movl	%eax, 28(%esp)
	movl	12(%ebp), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	movl	$LC3, 4(%esp)
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, 24(%esp)
	cmpl	$0, 24(%esp)
	jne	L6
	movl	$LC4, (%esp)
	call	_puts
	movl	$1, (%esp)
	call	_shit
	movsbl	%al, %eax
	jmp	L3
L6:
	movl	24(%esp), %eax
	movl	%eax, 12(%esp)
	movl	$131072, 8(%esp)
	movl	$1, 4(%esp)
	movl	$_memory, (%esp)
	call	_fread
	movl	24(%esp), %eax
	movl	%eax, (%esp)
	call	_fclose
	cmpl	$131072, 28(%esp)
	jbe	L7
	movl	$LC5, (%esp)
	call	_puts
	movl	$1, (%esp)
	call	_shit
	movsbl	%al, %eax
	jmp	L3
L7:
	movl	12(%ebp), %eax
	addl	$12, %eax
	movl	(%eax), %eax
	movl	%eax, (%esp)
	call	_atoi
	testl	%eax, %eax
	je	L8
	xorl	$3, 28(%esp)
L8:
	movl	28(%esp), %eax
	addl	$_memory, %eax
	movl	%eax, 4(%esp)
	movl	$LC6, (%esp)
	call	_scanf
	movl	12(%ebp), %eax
	addl	$4, %eax
	movl	(%eax), %eax
	movl	$LC7, 4(%esp)
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, 24(%esp)
	movl	24(%esp), %eax
	movl	%eax, 12(%esp)
	movl	$131072, 8(%esp)
	movl	$1, 4(%esp)
	movl	$_memory, (%esp)
	call	_fwrite
	movl	24(%esp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	$0, %eax
L3:
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE6:
	.section .rdata,"dr"
LC8:
	.ascii "Press Enter to exit.\0"
	.text
	.globl	_shit
	.def	_shit;	.scl	2;	.type	32;	.endef
_shit:
LFB7:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$40, %esp
	movl	8(%ebp), %eax
	movb	%al, -12(%ebp)
	movl	$LC8, (%esp)
	call	_puts
	movl	__imp___iob, %eax
	movl	%eax, (%esp)
	call	_fgetc
	movb	-12(%ebp), %al
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE7:
	.def	_puts;	.scl	2;	.type	32;	.endef
	.def	_atol;	.scl	2;	.type	32;	.endef
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_fread;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef
	.def	_atoi;	.scl	2;	.type	32;	.endef
	.def	_scanf;	.scl	2;	.type	32;	.endef
	.def	_fwrite;	.scl	2;	.type	32;	.endef
	.def	_fgetc;	.scl	2;	.type	32;	.endef

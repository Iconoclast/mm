# Flash Memory Editor for _The Legend of Zelda: Majora's Mask_

![](https://github.com/cxd4/mm/raw/master/flash/Zelda_Majoras_Mask/png/titlescr.png)

Insofar as the Nintendo 64 operating system, flash memory is one (and chronologically the last) of five supported attachments accessed by the Nintendo 64's peripheral interface (PI) as backup memory. It is also the least documented of them.

This is my first memory management project for flash memory, dedicated to my favorite game for the console. At the same time, I'm a little disappointed that, after over a decade of admiration for this game's predecessor (or "successor" for those biased enough), _The Legend of Zelda: Ocarina of Time_, so much work was put into hacking RAM and ROM disassembly but not utilizing the SRAM backup memory the game used via the PI, which could have contributed even more.

Maybe I don't have an eye for beauty in a real work of art, but I am proud to dismiss interest in this field for that game for its popularity in favor of the combinatorial rarities in the one I will reverse here. It is the only Nintendo 64 game to, all at once, require the expansion pak, access the flash device, have connections to the Nintendo 64DD, and truly answer "Project Reality" with vividness and sadness.

***

## Endians for Memory Devices

The Nintendo 64 PI will access flash memory, but the format in system RDRAM is different in **endianness** than that of the flash attachment. As consistent with the rest of the RDRAM, the blocks of data are in the big-endian order. You are used to that already, new as the term may sound, but the byte-endianness on the backup memory is little-endian.

Here, both endians are within the boundary of a 32-bit DWORD (four bytes), so, where big endianness results in A-B-C-D E-F-G-H order, little endianness results in D-C-B-A H-G-F-E, where each letter represents one byte of a DWORD. There are also impertinent techniques such as byte-swapping and word-swapping (correspondingly, A-B-C-D versus B-A-D-C and A-B-C-D versus C-D-A-B), which may be confused with "middle-endian" methods, but all that is of concern to managing the flash memory is making the file legible in the RDRAM endian and, when done, inverting the endian back to little for the game to read as flash data.

## Preparing the Flash Memory

To do an endian-swap on any ZS cart backup memory, use my `ZSENSWAP.EXE` utility. Have a file dump of the backup memory storage from the Nintendo 64 operating system ready, since a file system is obviously required to use the utilities here. You can use drag-and-drop to pass the file as a parameter to the endian swapper.

If the command succeeds, the console will print no debugging information, and the file you supplied will be modified correctly. If the command fails, the given file will not be updated, and a text message should have shown on the console. (On modern Microsoft Windows operating systems, the command-line environment does not always stay open after the program terminates.)

You might see any of these error messages:

* _"Command syntax missing domain."_ If you used drag-and-drop, are you sure you dragged a file on your disk onto the EXE file?
* _"Too many parameters specified."_ You somehow managed to pass several arguments for the command.
* _"Specified file was unreadable."_ Does the file exist? If you used drag-and-drop, maybe the file is in use by some other process.

In the future, since the current flash editor requires an exact section of the file to be specified (usually something like File 1 or File 2 in the USA game release), I should be able to auto-detect the endian and remove the external need for this application.  I've left the code in the separate program to reference it for a separate project I'll do later.

## Managing the Flash Memory

I'm sure most people only care about this part, but it took all of that information just to get here. Yes, I could have just written the editor to not care that the game wrote the flash memory as little-endian byte order, but I would have skewed the actual addressing offsets from the relative ones that correspond in RDRAM (no longer as easy to convert to GameShark cheats due to that). Providing my utility to endian-swap the file will help you analyze it by yourself or experiment with some hex editor thing.

Fortunately and unfortunately, since flash memory is the largest option for backup memory for the Nintendo 64, there are a ton of things you could change in the file to update something in some dimension of game progress. It will take me a while to implement support for at least the vast majority of variables loaded to the game, and with so much to configure and so little space to put it, I am not currently maintaining a user interface for using the editor. It is a command-line application.

> Everything you could need to know about invoking the save editor is in the [commands documentation](MANUAL).

## Current Issue

Since I'm on my own there isn't much need for a bug-tracking site or anything so formal; lots of issues will come and go as I do work here. The one I may not be able to handle for the longest time is language input while renaming files.

![NZSE (USA release of the ZS cart)](https://github.com/cxd4/mm/raw/master/flash/Zelda_Majoras_Mask/png/lang_usa.png)

![NZSP (Europen release)](https://github.com/cxd4/mm/raw/master/flash/Zelda_Majoras_Mask/png/lang_eur.png)

![NZSJ (original release)](https://github.com/cxd4/mm/raw/master/flash/Zelda_Majoras_Mask/png/lang_jap.png)

![(using the Hiragana alphabet](https://github.com/cxd4/mm/raw/master/flash/Zelda_Majoras_Mask/png/hiragana.png) ![(using the Katakana alphabet)](https://github.com/cxd4/mm/raw/master/flash/Zelda_Majoras_Mask/png/katakana.png)

#define FLASH_SIZE  0x00020000
#include <memory.h>
#include <stdio.h>
#include "zs.h"

int main(int argc, char *argv[])
{
    FILE *stream;
    register unsigned short i = 0x00000000;
    register unsigned short base = 0x00000000;
    register unsigned short checksum16 = 0x0000;

/* XD fuck that reminder, sick of looking at it */
    fopen_s(&stream, argv[1], "rb");
    if (stream == NULL)
    {
        printf("Specified file was unreadable.\n");
        return 1;
    }
    fread(memory, sizeof(unsigned char), FLASH_SIZE, stream);
    fclose(stream);

    if (argc == 2/*argv[2] == NULL*/)
    {
        printf("Memory block specifier absent.\n");
        return 1;
    }
    base  = argv[2][0] & 0x07; /* entry point into flash memory write */
    base  = base << 3;
    base |= argv[2][1] & 0x07;
    base  = base << 13;
    do
    {
        file[i] = memory[base | i];
        ++i;
    } while (i < 0x002000); /* finished importing the file */
    i = 0;
    switch (check_invalidity(file))
    {
        case -2:
            if (argv[3][0] == '-' && argv[3][1] == '-' && argv[3][2] == 'f' &&
                argv[3][3] == 'i' && argv[3][4] == 'l' && argv[3][5] == 'e' &&
                argv[3][6] == '-' && argv[3][7] == 'n' && argv[3][8] == 'e' &&
                argv[3][9] == 'w' && argv[3][10] == '\0')
			{
                i = 1; /* Do not print file overwrite warning. */
			}
			else
			{
                printf("File occupation label missing.\n");
                return 1;
            }
            break;
        case -1:
            printf("Corrupt additive checksum fix.\n");
            break;
    }

    if (argv[3] == NULL) /* (argv[3][0] == '\0') */
    { /* Do nothing; just break out and recalculate the checksum-16. */
    }
    else
    {
        char ASCII[64] = {
            'N','Z','S','*',' ','F','l','a','s','h',' ','E','d','i','t',0xA,
            '2','0','1','2','.','0','6','.','0','6','.','2','2','1','5',0xA,
            'T','o',' ','P','r','o','j','e','c','t',' ','L','i','n','k',0xA,
            'g','i','t','h','u','b','.','c','o','m','/','c','x','d','4',0xA };
        printf(ASCII);
        printf("https://raw.github.com/cxd4/mm/master/flash/zs/MANUAL\n");
        return 1;
    }

    i = 0x0000;
    do
    {
        checksum16 += file[i];
        ++i;
    } while (i < 0x100A);
    file[0x100A] = checksum16 >> 8;
    file[0x100B] = checksum16 & 0x00FF;
    do
    {
        checksum16 += file[i];
        ++i;
    } while (i < 0x138E);
    file[0x138E] = checksum16 >> 8;
    file[0x138F] = checksum16 & 0x00FF;

    for (i = 0x000000; i < 0x002000; ++i)
    {
        memory[base | i] = file[i];
    }
    fopen_s(&stream, argv[1], "wb");
    fwrite(memory, sizeof(unsigned char), FLASH_SIZE, stream);
    fclose(stream);
    return 0;
}

int check_invalidity(unsigned char file[0x2000])
{
    register unsigned int checksum16 = 0x0000, i = 0;
    char magic_string[7];
    magic_string[0] = file[0x0024];
    magic_string[1] = file[0x0025];
    magic_string[2] = file[0x0026];
    magic_string[3] = file[0x0027];
    magic_string[4] = file[0x0028];
    magic_string[5] = file[0x0029];
    magic_string[6] = file[0x002A];
    magic_string[7] = file[0x002B];
    if (!(magic_string[0] == 'Z' && magic_string[1] == 'E' && magic_string[2] ==
        'L' && magic_string[3] == 'D' && magic_string[4] == 'A' &&
        magic_string[5] == '3' && magic_string[6] == '\0'))
    {
        return -2;
    }

    if (file[0x100B] == 0)
    {
        if (file[0x138F] == 0)
        {
            return -1;
        }
        do
        {
            checksum16 += file[i];
            ++i;
        } while (i < 0x138E);
        i = (file[0x138E] << 8) | file[0x138F];
    }
    else
    {
        do
        {
            checksum16 += file[i];
            ++i;
        } while (i < 0x100A);
        i = (file[0x100A] << 8) | file[0x100B];
    }
    if (checksum16 == i)
    {
        return 0;
    }
    return -1;
}
unsigned char map_text(unsigned char Unicode)
{ /* The game does not use the ASCII to read in text digits. */
    register unsigned char i = 0x00;
    unsigned char code_page_eur[114] = {
        '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F',
        'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V',
        'W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l',
        'm','n','o','p','q','r','s','t','u','v','w','x','y','z',' ','-',
        '.',':'/*,'�','�','�','�','�','�','�','�','�','�','�','�','�','�',
        '�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�',
        '�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�',
        '�','�' */};
    unsigned char code_page_jap[236] = {
        '0','1','2','3','4','5','6','7','8','9', 0 , 0 , 0 , 0 , 0 , 0 , /* 0 */
         0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , /* 1 */
         0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , /* 2 */
         0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , /* 3 */
         0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , /* 4 */
         0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , /* 5 */
         0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , /* 6 */
         0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , /* 7 */
         0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , /* 8 */
         0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , /* 9 */
         0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,'A','B','C','D','E', /* A */
        'F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U', /* B */
        'V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k', /* C */
        'l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',' ', /* D */
        '~','?','!',':','-','(',')', 0 , 0 ,',','.','/' };               /* E */
    /*   0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F   /* F */
    if (file[0x100B] == 0)
    { /* checksum not specified at location appropriate for USA and PAL */
        if (Unicode == '\0') return 0xDF;
        do
        {
            if (code_page_jap[i] == Unicode) return (i);
            ++i;
        } while (i < 236);
    }
    else
    {
        if (Unicode == '\0') return 0x3F;
        do
        {
            if (code_page_eur[i] == Unicode) return (i);
            ++i;
        } while (i < 114);
    }
    return Unicode;
}
void text_copy(char str_dest[256], char str_src[256], unsigned char elements)
{
    register unsigned char byte = 0;
    while (byte < elements)
    {
        str_dest[byte] = str_src[byte];
        if (str_src[byte] == '\0') return;
        ++byte;
    }
}
unsigned short int text_to_bin(char ASCII[16])
{
    register unsigned short int binary = ASCII[0] & 1;
    if (ASCII [1] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII [1] & 1;
    if (ASCII [2] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII [2] & 1;
    if (ASCII [3] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII [3] & 1;
    if (ASCII [4] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII [4] & 1;
    if (ASCII [5] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII [5] & 1;
    if (ASCII [6] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII [6] & 1;
    if (ASCII [7] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII [7] & 1;
    if (ASCII [8] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII [8] & 1;
    if (ASCII [9] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII [9] & 1;
    if (ASCII[10] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII[10] & 1;
    if (ASCII[11] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII[11] & 1;
    if (ASCII[12] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII[12] & 1;
    if (ASCII[13] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII[13] & 1;
    if (ASCII[14] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII[14] & 1;
    if (ASCII[15] == '\0') return (binary);
    binary = binary << 1;
    binary |= ASCII[15] & 1;
    return (binary); /* Fucking loops can kiss my ass. */
}
short int text_to_dec(char ASCII[4])
{
    register short int decimal = ASCII[0] & 0x0F;
    if (ASCII[1] == '\0') return (decimal);
    if (ASCII[0] == 0x0D) /* '-' (negative sign) & 0x0F == 0x0D */
    {
        decimal  = -(ASCII[1] & 0x0F);
        if (ASCII[2] == '\0') return (decimal);
        decimal *= 10;
        decimal -= ASCII[2] & 0x0F;
        if (ASCII[3] == '\0') return (decimal);
        decimal *= 10;
        decimal -= ASCII[3] & 0x0F;
        return(decimal);
    }
    decimal *= 10;
    decimal += ASCII[1] & 0x0F;
    if (ASCII[2] == '\0') return (decimal);
    decimal *= 10;
    decimal += ASCII[2] & 0x0F;
    if (ASCII[3] == '\0') return (decimal);
    decimal *= 10;
    decimal += ASCII[3] & 0x0F;
    return (decimal);
}
unsigned short int text_to_hex(char ASCII[4])
{
    signed char digits[32] = {
        '\0' , 0x0A , 0x0B , 0x0C , 0x0D , 0x0E , 0x0F , '\0' ,
        '\0' , '\0' , '\0' , '\0' , '\0' , '\0' , '\0' , '\0' ,
        0x00 , 0x01 , 0x02 , 0x03 , 0x04 , 0x05 , 0x06 , 0x07 ,
        0x08 , 0x09 , '\0' , '\0' , '\0' , '\0' , '\0' , '\0' };
    register unsigned short int hexadecimal = digits[ASCII[0] & 0x1F];
    if (ASCII[1] == '\0') return (hexadecimal);
    hexadecimal = hexadecimal << 4;
    hexadecimal |= digits[ASCII[1] & 0x1F];
    if (ASCII[2] == '\0') return (hexadecimal);
    hexadecimal = hexadecimal << 4;
    hexadecimal |= digits[ASCII[2] & 0x1F];
    if (ASCII[3] == '\0') return (hexadecimal);
    hexadecimal = hexadecimal << 4;
    hexadecimal |= digits[ASCII[3] & 0x1F];
    return (hexadecimal);
}

/******************************************************************************\
* Project:  Endian Swapper for 128-KB Memory                                   *
* Release:  2012.03.16                                                         *
* License:  none (public domain)                                               *
* Creator:  R. Swedlow                                                         *
\******************************************************************************/
#include <stdio.h>
#define ENDIAN_NOSWAP       0b0000000000000000
#define ENDIAN_BYTESWAP     0b0000000000000001
#define ENDIAN_WORDSWAP     0b0000000000000010
#define ENDIAN_SWAP_DWORD   0b0000000000000011
#define ENDIAN_DWORDSWAP    0b0000000000000100
#define ENDIAN_SWAP_QWORD   0b0000000000000111
#define FLASH_SIZE          0x00020000
#define MEM_SIZE            FLASH_SIZE

static unsigned char buffer[MEM_SIZE];
static unsigned char output[MEM_SIZE];
/* Making MEM_SIZE dynamically controlled by the user supports any file. */

int main(int argc, char *argv[])
{
    FILE *stream;
    unsigned long int i = 0x00000000; /* 32-bit byte address index register */

    if (argv[1] == NULL)
    {
        printf("Command syntax missing domain.\n");
        return 1;
    }
    if (argc > 2)
    {
        printf("Too many parameters specified.\n");
        return 1;
    }
    stream = fopen(argv[1], "rb");
    if (stream == NULL)
    {
        printf("Specified file was unreadable.\n");
        return 1;
    }
    fread(buffer, sizeof(unsigned char), MEM_SIZE, stream);
    fclose(stream);

    do
    {
        output[i] = buffer[i ^ ENDIAN_SWAP_DWORD]; /* XOR(i, 0b00000011) */
        ++i; /* Invert the swap boundary bits for each index. */
    } while (i < MEM_SIZE); /* FOR loops are for... */

    stream = fopen(argv[1], "wb");
    fwrite(output, sizeof(unsigned char), MEM_SIZE, stream);
    fclose(stream);
    return 0;
}

/*
 * For C-style string formatting reasons, the following ASCII characters
 * were removed from the raw, binary storage of the game's text strings.
 *
 * 0000000 (NUL):  null-termination of highlighted, colored text phrases
 * 0000001 (SOH):  red text color (typical highlighting)
 * 0000010 (STX):  start of text string
 * 0000011 (ETX):  blue text color
 * 0000100 (EOT):  yellow text color
 * 0000110 (ACK):  purple text color
 * 0000111 (BEL):  silver text color
 * 0010001 (DC1):  line breaks to manually wrap game's captioning text
 * 0010010 (DC2):  pauses to continue printing text on controller input
 * ...others which were not important enough to have been encountered here.
 *
 * This file will only attempt to extract the ROM data strings accurately so
 * far as fixed line-wrapping is concerned; it makes no effort to convey
 * highlighted phrases or extra details like where exactly to find them.
 * For those, check zelda.wikia.com or some GameFAQs documents on quotes.
 *
 * The purpose of this file is to analyze internal ROM or game beta layout,
 * not to assist players with walkthrough- or FAQ-related material such as
 * where to reproduce the text or what exactly it means.
 */

/*
 * Here is a list of what all 20 Gossip Stones in the Moon dungeons will
 * clue you in on if you approach them while wearing the Mask of Truth.
 *
 * Incidentally, the order of these response text strings matches not only
 * their strict ordering within the ROM data but also the exact order in
 * which mask items are enumerated for binary storage to the Masks Subscreen.
 */

const char * Moon_Gossip_Stone_mask_tips[20] = {
    /* LINK */
    "It seems the one cursed by the\n"\
    "strange, sparkling gold spiders had\n"\
    "the Mask of Truth...",

    /* LINK */
    "It seems his mother, Madame\n"\
    "Aroma, had Kafei's Mask...",

    /* LINK */
    "It seems the All-Night Mask was\n"\
    "being sold at the Curiosity Shop...",

    /* LINK */
    "It seems Grog of the Cucco Shack\n"\
    "had the Bunny Hood...",

    /* LINK */
    "It seems the owner of the\n"\
    "Curiosity Shop was keeping\n"\
    "the Keaton Mask...",

    /* DEKU */
    "It seems the Gorman Brothers\n"\
    "were using Garo's Mask for ill...",

    /* DEKU */
    "It seems Cremia, the owner of\n"\
    "Romani Ranch, had Romani's Mask...",

    /* DEKU */
    "It seems the leader of the Gorman\n"\
    "Troupe had the Circus Leader's\n"\
    "Mask...",

    /* DEKU */
    "It seems the postman had the\n"\
    "Postman's Hat...",

    /* DEKU */
    "It seems Kafei and Anju had the\n"\
    "Couple's Mask...",

    /* GORON */
    "It seems the Great Fairy in town\n"\
    "had the Great Fairy's Mask...",

    /* GORON */
    "It seems the Gibdo Mask could\n"\
    "fall from Pamela's father's cursed\n"\
    "face...",

    /* GORON */
    "It seems the hungry Goron was\n"\
    "wearing Don Gero's Mask...",

    /* GORON */
    "It seems Kamaro, the spirit\n"\
    "dancer, had Kamaro's Mask...",

    /* GORON */
    "It seems Skull Keeta, Captain of\n"\
    "the Skull Knights, had the\n"\
    "Captain's Hat...",

    /* ZORA */
    "It seems Shiro, the unseen stone\n"\
    "soldier, had the Stone Mask...",

    /* ZORA */
    "It seems Guru-Guru, the traveling\n"\
    "musician, had the Bremen Mask...",

    /* ZORA */
    "It seems the old woman with the\n"\
    "Bomb Bag had the Blast Mask...",

    /* ZORA */
    "It seems the Deku Scrub butler\n"\
    "had the Mask of Scents...",

    /* ZORA */
    "It seems the Giant's Mask was\n"\
    "dormant in Stone Tower Temple...",
};

/*
 * For faster analysis, here is a version of the list without the lines cut.
 */
const char * Moon_Gossip_Stone_mask_tips_nomultiline[20] = {
    "It seems the one cursed by the strange, sparkling gold spiders had the "\
      "Mask of Truth...",
    "It seems his mother, Madame Aroma, had Kafei's Mask...",
    "It seems the All-Night Mask was being sold at the Curiosity Shop...",
    "It seems Grog of the Cucco Shack had the Bunny Hood...",
    "It seems the owner of the Curiosity Shop was keeping the Keaton Mask...",
    "It seems the Gorman Brothers were using Garo's Mask for ill...",
    "It seems Cremia, the owner of Romani Ranch, had Romani's Mask...",
    "It seems the leader of the Gorman Troupe had the Circus Leader's Mask...",
    "It seems the postman had the Postman's Hat...",
    "It seems Kafei and Anju had the Couple's Mask...",
    "It seems the Great Fairy in town had the Great Fairy's Mask...",
    "It seems the Gibdo Mask could fall from Pamela's father's cursed face...",
    "It seems the hungry Goron was wearing Don Gero's Mask...",
    "It seems Kamaro, the spirit dancer, had Kamaro's Mask...",
    "It seems Skull Keeta, Captain of the Skull Knights, had the Captain's "\
      "Hat...",
    "It seems Shiro, the unseen stone soldier, had the Stone Mask...",
    "It seems Guru-Guru, the traveling musician, had the Bremen Mask...",
    "It seems the old woman with the Bomb Bag had the Blast Mask...",
    "It seems the Deku Scrub butler had the Mask of Scents...",
    "It seems the Giant's Mask was dormant in Stone Tower Temple...",
};

/*
 * The array of strings above, in addition to the next one below, leads to at
 * least one of two possible conclusions:
 *     1.  Both string lists agree on an internally accurate order of the
 *         masks (in contrast to alphabetizing their names or going by
 *         chronology), which coincides with their IDs in the subscreens.
 *     2.  The next string list was the older, beta array of Gossip Stone
 *         clues about where to find the masks but was replaced with the
 *         former list above to make the exact mask names more defined.
 * In either case, none of the text in the following array of strings have
 * been found from any Gossip Stones, either on the moon or across Termina.
 */

const char * Termina_Gossip_Stone_mask_tips[20] = {
    /* Mask of Truth */
    "The mask that sees into people's\n"\
    "hearts seems to be near the\n"\
    "strange, shining, gold spiders...",

    /* Kafei's Mask */
    "The cute boy's mask seems to\n"\
    "have been made by an important\n"\
    "man's wife...",

    /* All-Night Mask */
    "The weird mask that disrupts\n"\
    "sleeping habits seems to be \n"\
    "found in a suspicious shop that\n"\
    "opens only at night...",

    /* Bunny Hood */
    "It seems the animal-loving young\n"\
    "man with the scary face but\n"\
    "kind heart has the wild ears that\n"\
    "hear well...",

    /* Keaton Mask */
    "It seems an animal mask that was\n"\
    "popular with children long ago is\n"\
    "being cherished by the owner of\n"\
    "the suspicious shop...",

    /* Garo's Mask */
    "The suspicious brothers seem to\n"\
    "have a mask once used for spying\n"\
    "activities...",

    /* Romani's Mask */
    "It seems the girl who smells of\n"\
    "the ranch has a mask that only\n"\
    "adults have...",

    /* Circus Leader's Mask */
    "The mask that trickles out\n"\
    "troubles from its face seems to\n"\
    "be held by the greatest of\n"\
    "traveling men...",

    /* Postman's Hat */
    "It seems the person who is\n"\
    "conscientious about being on\n"\
    "time...\n"\ /* ASCII DC2 for continuing on the next page */
    "can see into the boxes that\n"\
    "enable people to keep in touch\n"\
    "with other people's feelings.",

    /* Couple's Mask */
    "It seems the two who have most\n"\
    "reason to have it are indeed the\n"\
    "ones who have the mask that is\n"\
    "full of a man and woman's love...",

    /* Great Fairy's Mask */
    "A large and colorful being seems\n"\
    "to have a mask that calms those\n"\
    "scattered in temples...",

    /* Gibdo Mask */
    "It seems the father of the girl\n"\
    "who's devoted to her parent is\n"\
    "being forced to wear a frightening\n"\
    "mask...",

    /* Don Gero's Mask */
    "He who is troubled by cold and\n"\
    "hunger seems to have a mask\n"\
    "that gathers voices to sing...",

    /* Kamaro's Mask */
    "The dancer's spirit that appears\n"\
    "night after night in the great field\n"\
    "seems to have a mask which\n"\
    "causes one to dance.",

    /* Captain's Hat */
    "It seems the mystical item that\n"\
    "the skulls obey is in the fiercely\n"\
    "burning flame in the graveyard of\n"\
    "an accursed land...",

    /* Stone Mask */
    "It seems a man so inconspicuous\n"\
    "he can be seen only through the\n"\
    "Lens of Truth has a mask which\n"\
    "also is completely inconspicuous...",

    /* Bremen Mask */
    "He who plays music as he travels\n"\
    "about seems to have a mask\n"\
    "that animals follow obediently...",

    /* Blast Mask */
    "The old woman with knowledge of\n"\
    "explosives has a dangerous mask\n"\
    "filled with gunpowder...",

    /* Mask of Scents */
    "He of high class and manners who\n"\
    "lives in the swamp has a useful\n"\
    "mask that distinguishes scents...",

    /* Giant's Mask */
    "A mask that contains gigantic\n"\
    "power seems to be resting in the\n"\
    "temple of the accursed land...",
};

/*
 * In addition to the Gossip Stone clues listed above for masks, there are
 * some other text strings attached to known Gossip Stones across Termina
 * that give more specifics about these masks (in addition to one Gossip
 * Stone that acknowledges the Mask of Fierce Deity), but there is no
 * meaning to extract that string array here because it is interleaved with
 * non-mask-related advice in between clues that are about masks.  Therefore,
 * it is not organized enough to depict the allocation of masks in the game.
 */

/*
 * Instead, next is a list of the Happy Mask Salesman's various reactions
 * to seeing Link wear each of the masks.  Their order does remain a mystery.
 */
const char * Happy_Mask_Salesman_mask_reactions[20] = {
 /* "That's the Great Fairy's Mask,\n"\ */
 /* "isn't it?" */
    "That is quite rare.\n"\
    "Normally, you wouldn't be able\n"\
    "to get one of those.",

 /* "That's the Gibdo Mask, isn't it?" */
    "That is a fine mask. It is filled\n"\
    "with the love of a father and\n"\
    "child.",

 /* "That is the Mask of Truth,\n"\ */
 /* "isn't it?" */
    "You have a frightful mask. But\n"\
    "being able to see into people's\n"\
    "hearts and minds seems useful...",

 /* "That's the Giant's Mask, isn't it?" */
    "That truly is a rare mask...",

 /* "That's Kafei's Mask, isn't it?" */
    "The worry of a concerned mother\n"\
    "is expressed quite well in that\n"\
    "mask.",

 /* "That's Don Gero's Mask,\n"\ */
 /* "isn't it?" */
    "That mask has a very operatic\n"\
    "feel about it.",

 /* "That's the Blast Mask, isn't it?" */
    "That is a fine mask.\n"\
    "It is filled with feelings of\n"\
    "gratitude.",

 /* "That's the Couple's Mask, isn't it?" */
    "That is truly a very fine mask.\n"\
    "It's overflowing with powerful\n"\
    "feelings of love and gratitude.",

 /* "That is the Mask of Scents,\n"\ */
 /* "isn't it?" */
    "You've done well to get that mask.\n"\
    "Looking at its condition, I can tell\n"\
    "that its previous owner was neat\n"\
    "and tidy.",

 /* "That's Kamaro's Mask, isn't it?", */
    "That is a fine mask.\n"\
    "It is filled with the joy a teacher\n"\
    "has in finding a good student.",

 /* "That's the Stone Mask, isn't it?" */
    "That is a fine mask.\n"\
    "It is filled with an appreciation\n"\
    "for things that go unnoticed.",

 /* "That's the Postman's Hat, isn't it?" */
    "That is a fine thing.\n"\
    "It is filled with the joy of\n"\
    "freedom.",

 /* "That's the Bunny Hood, isn't it?" */
    "That is a fine thing.\n"\
    "It is filled with kindness towards\n"\
    "animals.",

 /* "That's the Captain's Hat, isn't it?" */
    "That is a splendid thing. It earns\n"\
    "the respect and sworn allegiance\n"\
    "of legions of soldiers.",

 /* "That is the Bremen Mask, isn't it?" */
    "If you have that, then it would\n"\
    "appear you have the qualities of a\n"\
    "leader.",

 /* "That's the Circus Leader's Mask,\n"\ */
 /* "isn't it?" */
    "That is a fine mask. It is filled\n"\
    "with the feelings of tenderness\n"\
    "left behind in the back of one's\n"\    "heart.",

 /* "That's a Keaton Mask, isn't it?" */
    "It carries a lot of nostalgia. It\n"\
    "was popular with children long\n"\
    "ago.",

 /* "That's the Garo's Mask, isn't it?" */
    "If you have that mask, you must\n"\
    "be one with quite a bit of\n"\
    "courage.",

 /* "That's the All-Night Mask, isn't it?" */
    "That looks like an expensive mask.",

 /* "That's Romani's Mask, isn't it?" */
    "That is a nice mask.\n"\
    "It is a sentimental item that\n"\
    "exudes a lot of maturity.",
};

/*
 * In addition to storing an array of strings about mask tips in Gossip
 * Stones, the game also organizes an array of strings about event
 * completions in the Bombers' Notebook, which does not seem to go in order
 * of the people registered to the notebook entries.  However, it also does
 * not seem to be 100%, completely in the order of the masks' ID numbers.
 *
 * Here is the full excerpt of strings about masks with nothing removed in
 * between but a few non-mask-related entries between the All-Night Mask and
 * the Bunny Hood.  Obviously all 20 of the masks cannot be listed here
 * because a few of them are completely unrelated to notebook events.
 */

const char * Bombers_Notebook_mask_entries[13] = {
    "Romani's Mask         Romani Ranch\n"\
    "Shows Cremia considers you an adult",

    "Keaton Mask              Backroom\n"\
    "Kafei and Curiosity Shop's mask",

    "Kafei's Mask  Mayor's Drawing Room\n"\
    "Show it to help with the search",

    "All-Night Mask    Curiosity Shop\n"\
    "Strange mask at a bargain. Valuable.",

    "Bunny Hood             Cucco Shack\n"\
    "Thanks for turning chicks into cuccos",

    "Garo's Mask          Gorman Track\n"\
    "Prize for winning Gorman horserace",

    "Circus Leader's Mask     Milk Bar\n"\
    "Thanks for moving Gorman with song",

    "Postman's Hat  Town: Near Milk Bar\n"\
    "Thanks for enabling him to flee",

    "Couple's Mask  Inn's Employee Room\n"\
    "Sign of Anju and Kafei's happiness",

    "Blast Mask           North of Town\n"\
    "Thanks for guarding bag from thief",

    "Kamaro's Mask        Termina Field\n"\
    "Sign of taking over for ghost dancer",

    "Stone Mask           Road to Ikana\n"\
    "Thanks for giving strength to a soldier",

    "Bremen Mask    Town Laundry Pool\n"\
    "Thanks for hearing confession",
 };
 /*
  * (While it appears to be mostly in mask order, there are some conflicts.)
  */

/*
 * A more obvious place to look is in the game's string table for object
 * descriptions and information while browsing the subscreens.  For all
 * mask objects, the portion of the string table is not interrupted, and
 * its excerpt is given below which is in the "internally accurate" order.
 */

const char * subscreen_mask_information[24] = {
    "Deku Mask\n"\
    "Wear it with � to assume Deku\n"\
    "form. Use � to change back.",

    "Goron Mask\n"\
    "Wear it with � to assume Goron\n"\
    "form. Use � to change back.",

    "Zora Mask\n"\
    "Wear it with � to assume Zora\n"\
    "form. Use � to change back.",

    "Fierce Deity's Mask\n"\
    "Wear it with �. Its dark power\n"\
    "can be used only in boss rooms.",

    "Mask of Truth\n"\
    "Wear it to read the thoughts of\n"\
    "Gossip Stones and animals.",

    "Kafei's Mask\n"\
    "Wear it with � to inquire about\n"\
    "Kafei's whereabouts.",

    "All-Night Mask\n"\
    "When you wear it with �, you\n"\
    "don't get sleepy.",

    "Bunny Hood\n"\
    "Wear it with � to be filled with\n"\
    "the speed and hearing of the wild.",

    "Keaton Mask\n"\
    "The mask of the ghost fox,\n"\
    "Keaton. Wear it with �.",

    "Garo's Mask\n"\
    "This mask can summon the hidden\n"\
    "Garo ninjas. Wear it with �.",

    "Romani's Mask\n"\
    "Wear it with � to show you're a\n"\
    "member of the Milk Bar, Latte.",

    "Circus Leader's Mask\n"\
    "People related to Gorman will\n"\
    "react to this.",

    "Postman's Hat\n"\
    "You can look in mailboxes when\n"\
    "you wear this with �.",

    "Couple's Mask\n"\
    "When you wear it with �, you\n"\
    "can soften people's hearts.",

    "Great Fairy's Mask \n"\ /* Here, the added space ' ' was honest error. */
    "The mask's hair will shimmer when\n"\
    "you're close to a Stray Fairy.",

    "Gibdo Mask\n"\
    "Use it with �. Even a real Gibdo\n"\
    "will mistake you for its own kind.",

    "Don Gero's Mask\n"\
    "When you wear it, you can call\n"\
    "the Frog Choir members together.",

    "Kamaro's Mask\n"\
    "Wear this with � to perform a\n"\
    "mysterious dance.",

    "Captain's Hat\n"\
    "Wear it with � to pose as\n"\
    "Captain Keeta.",

    "Stone Mask\n"\
    "Become as plain as stone so you\n"\
    "can blend into your surroundings.",

    "Bremen Mask\n"\
    "Wear it with � so young animals\n"\
    "will mistake you for their leader.",

    "Blast Mask\n"\
    "Wear it with �, then detonate\n"\
    "it with �...\n"\
 /* device control 2 interrupt */
    "Losing hearts from the explosion\n"\
    "is a side effect.",

    "Mask of Scents\n"\
    "Wear it with � to heighten your\n"\
    "sense of smell.",

    "Giant's Mask\n"\
    "If you wear it in a certain room,\n"\
    "you'll grow into a giant.",
};

/*
 * Finally, here is a list of the mask texts printed upon first acquiring
 * each mask, with the bulk of the text removed for simplification of the
 * ordering objective.  (That is, only the first statement of each string
 * is shown here.)  Here, again, the meaning behind the order is not clear.
 */

const char * mask_acquisition_headers[24] = {
    "You got the Deku Mask to keep\n"\
      "as a memento!",
    "You got the Goron Mask!",
    "You got the Zora Mask!",
    "You got the Fierce Deity's Mask!",
    "You got the Captain's Hat!",
    "You got the Giant's Mask!",
    "You got the All-Night Mask!",
    "You got the Bunny Hood!",
    "You got the Keaton Mask!",
    "You got the Garo's Mask!",
    "You got Romani's Mask!",
    "You got the Circus Leader's\n"\
      "Mask!",
    "You got the Postman's Hat!",
    "You got the Couple's Mask!",
    "You got the Great Fairy Mask!",
    "You got the Gibdo Mask!",
    "You got Don Gero's Mask!",
    "You got Kamaro's Mask!",
    "You got the Mask of Truth!",
    "You got the Stone Mask!",
    "You got the Bremen Mask!",
    "You got the Blast Mask!",
    "You got the Mask of Scents!",
    "You've been given Kafei's Mask\n"\
      "and recruited to locate a missing\n"\
      "person!",
};

/*
 * In conclusion, clearly the game's mask enumeration was not very well-
 * engineered, or the identification constants attached to each mask would
 * have kept consistent for all of these references to enforce data sharing
 * and recycling to use less data.  The only pattern or ordering of these
 * masks that seems to be applied in more than one of these tables is the
 * one that matches the mask ID numbers that register their subscreen stores.
 */

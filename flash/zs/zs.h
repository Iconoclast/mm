static unsigned char memory[FLASH_SIZE];
static unsigned char file[0x002000]; /* each save file is 8 kilobytes */

unsigned char map_text(unsigned char Unicode);
void text_copy(char str_dest[256], char str_src[256], unsigned char elements);
short int text_to_dec(char ASCII[4]);
unsigned short int text_to_bin(char ASCII[16]);
unsigned short int text_to_hex(char ASCII[4]);
int check_invalidity(unsigned char file[0x2000]);

void NOP(register int word) {
    word = word ^ word;
    return;
}
void bank(register int word) {
    file[0x0EDE] = (unsigned char)(word >> 8);
    file[0x0EDF] = word & 0x00FF;
    return;
}
void boss_masks(register int word) {
    file[0x00BF] &= 0xF0;
    file[0x00BF] |= word;
    return;
}
void day(register int word) {
    file[0x001B] = (unsigned char)word;
    return;
}
void fairy(register int word) {
    file[0x0022] = (unsigned char)word;
    return;
}
void file_copy(register int word) {
    register int i = 0;
    do
    {
        memory[word | i] = file[i];
        ++i;
    } while (i != 0x002000);
    return;
}
void file_erase(register int word) {
    register int i = 0;
    word = word << 13;
    do
    {
        file[i] = 0x00; /* Erase default octets. */
        memory[word | i | 0x002000] = 0x00; /* Erase backup octets. */
        ++i;
    } while (i != 0x002000);
    return;
}
void file_new(register int word) {
    memset(file, 0x00, 0x2000); /* erase file just as game would */
    file[0x0024] = 'Z'; /* minimum requirement for in-use files */
    file[0x0025] = 'E'; /* minimum requirement for in-use files */
    file[0x0026] = 'L'; /* minimum requirement for in-use files */
    file[0x0027] = 'D'; /* minimum requirement for in-use files */
    file[0x0028] = 'A'; /* minimum requirement for in-use files */
    file[0x0029] = '3'; /* minimum requirement for in-use files */
    if (word != 0) /* argument five defined by end user */
    {
        file[0x0002] = 0x1C; /* mysterious forest to Termina */
        file[0x000C] = 0x55; /* start time in Termina under a new file */
        file[0x000D] = 0x55; /* 0x5555 is 07:59:59.560546875 AM */
        file[0x0020] = 0x04; /* normal Link, no transformation */
        file[0x0035] = 0x30; /* zs data.bin 0 --life-energy-reservoire 3 */
        file[0x0037] = 0x30; /* zs data.bin 0 --life-energy 0030 */
        file[0x0039] = 0x30; /* zs data.bin 0 --magic-power 0030 */
        file[0x006D] = 0x11; /* Kokiri Sword, Hylian Shield */
        memset(file + 0x0070, 0xFF, 48); /* clear masks/items inventory */
    } /* Give Link the usual shit the game does when making new files. */
    return;
}
void file_rename(register int word) {
    char name[8] = "FUCK!"; /* can't convert 8-byte string to int reg... */
    file[0x002C] = map_text(name[0]); //need
    file[0x002D] = map_text(name[1]); //to
    file[0x002E] = map_text(name[2]); //find
    file[0x002F] = map_text(name[3]); //good
    file[0x0030] = map_text(name[4]); //way
    file[0x0031] = map_text(name[5]); //to
    file[0x0032] = map_text(name[6]); //fix
    file[0x0033] = map_text(name[7]); //this
    word = word ^ word;
    return;
}
void file_switch(register int word) {
    register int i = 0;
    unsigned char placeholder[0x2000] = "";
    word = word << 13;
    do
    {
        placeholder[i] = file[i];
        ++i;
    } while (i != 0x002000);
    i = 0x000000;
    do
    {
        file[i] = memory[word | i];
        ++i;
    } while (i != 0x002000);
    i = 0x000000;
    do
    {
        memory[word | i] = placeholder[i];
        ++i;
    } while (i != 0x002000);
    return;
}
void form(register int word) {
    file[0x0020] = (unsigned char)word;
    return;
}
void gate(register int word) {
    file[0x0F04] &= 0xDF;
    word = word ^ word;
    return;
}
void heart_assembly(register int word) {
    word = word << 4;
    file[0x00BC] &= 0x0F;
    file[0x00BC] |= word;
    return;
}
void heart_piece(register int word) {
    switch (word) {
        case  1:
            file[0x0D2E] ^= 0x04;
            break;
        case  4:
            file[0x0D12] ^= 0x04;
            break;
        case  5:
            file[0x0F45] ^= 0x01;
            break;
        case  6:
            file[0x0F37] ^= 0x20;
            break;
        case 13:
            file[0x0F33] ^= 0x08;
            file[0x0F34] ^= 0x01; /* byte to recall bank deposit rewards */
            break;
        default:
            printf("Specified heart piece number invalid?\n");
            return/* 1*/;
    }
    return;
}
void inv_b_button(register int word) {
    file[0x004C] = (unsigned char)word;
    return;
}
void inv_c_down(register int word) {
    file[0x004E] = (unsigned char)word;
    return;
}
void inv_c_left(register int word) {
    file[0x004D] = (unsigned char)word;
    return;
}
void inv_c_right(register int word) {
    file[0x004F] = (unsigned char)word;
    return;
}
void inv_subscreen(register int word) {
/* #define SUBSCREEN_ENTRY_POINT (word >> 8) + 0x0070 */
    file[0x0070 + ((word >> 8) % 48)] = word & 0x00FF;
    return;
}
void item_arrows(register int word) {
    file[0x00A1] = (unsigned char)word;
    return;
}
void item_bombchu(register int word) {
    file[0x00A7] = (unsigned char)word;
    return;
}
void item_bombs(register int word) {
    file[0x00A6] = (unsigned char)word;
    return;
}
void item_deku_nuts(register int word) {
    file[0x00A9] = (unsigned char)word;
    return;
}
void item_deku_sticks(register int word) {
    file[0x00A8] = (unsigned char)word;
    return;
}
void item_magic_beans(register int word) {
    file[0x00AA] = (unsigned char)word;
    return;
}
void item_pictograph(register int word) {
    file[0x00BC] = (unsigned char)word;
    return;
}
void item_powder_kegs(register int word) {
    file[0x00AC] = (unsigned char)word;
    return;
}
void life_energy(register int word) {
    file[0x0036] = (unsigned char)(word >> 8);
    file[0x0037] = word & 0x00FF;
    return;
}
void life_energy_reservoire(register int word) {
    file[0x0034] = (unsigned char)(word >> 8);
    file[0x0035] = word & 0x00FF;
    return;
}
void magic_power(register int word) {
    file[0x0039] = (unsigned char)word;
    return;
}
void owl_spot(register int word) {
    FILE *shit;
    register int i = 0;
    register int checksum16 = 0;
    file[0x000E] = (unsigned char)(word >> 8);
    file[0x000F] = word & 0x00FF;
    file[0x001F] = 0x01; /* possible Boolean flag for owl saves? */
    file[0x0023] = 0x01; /* possible Boolean flag for owl saves? */
    do
    {
        checksum16 += file[i];
        ++i;
    } while (i != 0x100A);
    file[0x100A] = (unsigned char)(checksum16 >> 8);
    file[0x100B] = checksum16 & 0x00FF;
    file[0x1010] = 0x04; /* ? */
    file[0x1012] = 0x20; /* ? */

    i = 0x0000;
    do
    {
        memory[i | 0x008000] = file[i]; /* File 1 */
        memory[i | 0x00C000] = file[i]; /* File 1 */
        memory[i | 0x010000] = file[i]; /* File 2 */
        memory[i | 0x014000] = file[i]; /* File 2 */
        ++i;
    } while (i < 0x002000);
    fopen_s(&shit, "Zelda Majora's Mask.fla", "wb");
    fwrite(memory, sizeof(unsigned char), FLASH_SIZE, shit);
    fclose(shit);
    return/* 0*/;
}
void owl_statues(register int word) {
    file[0x0046] = (unsigned char)(word >> 8);
    file[0x0047] = word & 0x00FF;
    return;
}
void rupees(register int word) {
    file[0x003A] = (unsigned char)(word >> 8);
    file[0x003B] = word & 0x00FF;
    return;
}
void small_keys_Great_Bay(register int word) {
    file[0x00CC] = (unsigned char)word;
    return;
}
void small_keys_Snowhead(register int word) {
    file[0x00CB] = (unsigned char)word;
    return;
}
void small_keys_Stone_Tower(register int word) {
    file[0x00CD] = (unsigned char)word;
    return;
}
void small_keys_Woodfall(register int word) {
    file[0x00CA] = (unsigned char)word;
    return;
}
void songs(register int shit) {
    register unsigned int word = (unsigned) shit;
    word = word << 6; /* Only the high-order two bits at 0x00BF store melodies. */
    file[0x00BF] &= 0x3F;
    file[0x00BF] |= word & 0x40; /* Sonata of Awakening */
    file[0x00BF] |= word & 0x80; /* Goron Lullaby */
    word = word >> 8;
    file[0x00BE]  = word & 0x01; /* New Wave Bossa Nova */
    file[0x00BE] |= word & 0x02; /* Elegy of Emptiness */
    file[0x00BE] |= word & 0x04; /* Oath to Order */
    file[0x00BE] |= word & 0x08; /* Saria's Song (beta) */
    file[0x00BE] |= word & 0x10; /* Song of Time */
    file[0x00BE] |= word & 0x20; /* Song of Healing */
    file[0x00BE] |= word & 0x40; /* Epona's Song */
    file[0x00BE] |= word & 0x80; /* Song of Soaring */
    word = word >> 8;
    file[0x00BD] &= 0xFC; /* binary flush before OR-bit-masking */
    file[0x00BD] |= word & 0x01; /* Song of Storms */
    file[0x00BD] |= word & 0x02; /* Sun's Song (beta) */
    word = word >> 2;
    file[0x00BC] &= 0xF0; /* binary flush before OR-bit-masking */
    file[0x00BC] |= word & 0x01; /* Lullaby Intro (to Goron's Lullaby) */
    file[0x00BC] |= word & 0x02; /* reserved (does not exist) */
    file[0x00BC] |= word & 0x04; /* reserved (does not exist) */
    file[0x00BC] |= word & 0x08; /* reserved (does not exist) */
    return;
}
void stray_fairies_Great_Bay(register int word) {
    file[0x00D6] = (unsigned char)word;
    return;
}
void stray_fairies_Snowhead(register int word) {
    file[0x00D5] = (unsigned char)word;
    return;
}
void stray_fairies_Stone_Tower(register int word) {
    file[0x00D7] = (unsigned char)word;
    return;
}
void stray_fairies_Woodfall(register int word) {
    file[0x00D4] = (unsigned char)word;
    return;
}
void temple_Great_Bay(register int word) {
    file[0x00C2] = (unsigned char)word;
    return;
}
void temple_Snowhead(register int word) {
    file[0x00C1] = (unsigned char)word;
    return;
}
void temple_Stone_Tower(register int word) {
    file[0x00C3] = (unsigned char)word;
    return;
}
void temple_Woodfall(register int word) {
    file[0x00C0] = (unsigned char)word;
    return;
}
void time(register int word) {
    file[0x000C] = (unsigned char)(word >> 8);
    file[0x000D] = word & 0x00FF;
    /* checksum16 = 0x0024; // Re-define base checksum for owl statue data. */
    return;
}
void time_rate(register int word) {
    file[0x0014] = 0xFF;
    file[0x0015] = 0xFF;
    file[0x0016] = (unsigned char)(word >> 8);
    file[0x0017] = word & 0x00FF;
    return;
}
void wallet(register int word) {
    word = word & 0x03;
    word = word << 4;
    file[0x00BA] = (unsigned char)word;
    return;
}

int parse_arguments(char* argument)
{
    enum
    {
        NOP,
        FILE_NEW, /* 0x0000:0x1FFF */
        FILE_COPY, /* 0x0000:0x1FFF */
        FILE_ERASE, /* 0x0000:0x1FFF */
        FILE_SWITCH, /* 0x0000:0x1FFF */
        TIME, /* 0x000C:0x000D */
        OWL_SPOT, /* 0x000E:0x000F */
        TIME_RATE, /* 0x0014:0x0017 */
        DAY, /* 0x001B */
        FORM, /* 0x0020 */
        FAIRY, /* 0x0022 */
        FILE_RENAME, /* 0x002C:0x0033 */
        LIFE_ENERGY_RESERVOIRE, /* 0x0034:0x0035 */
        LIFE_ENERGY, /* 0x0036:0x0037 */
        MAGIC_POWER, /* 0x0039 */
        RUPEES, /* 0x003A:0x003B */
        OWL_STATUES, /* 0x0046:0x0047 */
        INV_B_BUTTON, /* 0x004C */
        INV_C_LEFT, /* 0x004D */
        INV_C_DOWN, /* 0x004E */
        INV_C_RIGHT, /* 0x004F */
        INV_SUBSCREEN, /* 0x0070:0x009F */
        ITEM_ARROWS, /* 0x00A1 */
        ITEM_BOMBS, /* 0x00A6 */
        ITEM_BOMBCHU, /* 0x00A7 */
        ITEM_DEKU_STICKS, /* 0x00A8 */
        ITEM_DEKU_NUTS, /* 0x00A9 */
        ITEM_MAGIC_BEANS, /* 0x00AA */
        ITEM_POWDER_KEGS, /* 0x00AC */
        WALLET, /* 0x00BA */
        HEART_ASSEMBLY, /* 0x00BC (highest-order four bits) */
        ITEM_PICTOGRAPH, /* 0x00BC */
        SONGS, /* 0x00BC:0x00BF (only some of the middle bits) */
        BOSS_MASKS, /* 0x00BF (lowest-order four bits) */
        TEMPLE_WOODFALL, /* 0x00C0 */
        TEMPLE_SNOWHEAD, /* 0x00C1 */
        TEMPLE_GREAT_BAY, /* 0x00C2 */
        TEMPLE_STONE_TOWER, /* 0x00C3 */
        SMALL_KEYS_WOODFALL, /* 0x00CA */
        SMALL_KEYS_SNOWHEAD, /* 0x00CB */
        SMALL_KEYS_GREAT_BAY, /* 0x00CC */
        SMALL_KEYS_STONE_TOWER, /* 0x00CD */
        STRAY_FAIRIES_WOODFALL, /* 0x00D4 */
        STRAY_FAIRIES_SNOWHEAD, /* 0x00D5 */
        STRAY_FAIRIES_GREAT_BAY, /* 0x00D6 */
        STRAY_FAIRIES_STONE_TOWER, /* 0x00D7 */
        BANK, /* 0x0EDE:0x0EDF */
        GATE, /* 0x0F04 */
        HEART_PIECE /* unknown and incomplete, ~= 0x0D2E:0x0F34 ? */
    };

    struct
    {
        char *name;
        int argc;
        int reserved;
        int index;
    } action[64] = {
        {"", 0, 0x0000, NOP},
        {"--file-new", 0, 0x0000, FILE_NEW},
        {"--file-copy", 0, 0x0000, FILE_COPY},
        {"--file-erase", 0, 0x0000, FILE_ERASE},
        {"--file-switch", 0, 0x0000, FILE_SWITCH},
        {"--time", 0, 0x0000, TIME},
        {"--owl-spot", 0, 0x0000, OWL_SPOT},
        {"--time-rate", 0, 0x0000, TIME_RATE},
        {"--day", 0, 0x0000, DAY},
        {"--form", 0, 0x0000, FORM},
        {"--fairy", 0, 0x0000, FAIRY},
        {"--file-rename", 0, 0x0000, FILE_RENAME},
        {"--life-energy-reservoire", 0, 0x0000, LIFE_ENERGY_RESERVOIRE},
        {"--life-energy", 0, 0x0000, LIFE_ENERGY},
        {"--magic-power", 0, 0x0000, MAGIC_POWER},
        {"--rupees", 0, 0x0000, RUPEES},
        {"--owl-statues", 0, 0x0000, OWL_STATUES},
        {"--inv-b-button", 0, 0x0000, INV_B_BUTTON},
        {"--inv-c-left", 0, 0x0000, INV_C_LEFT},
        {"--inv-c-down", 0, 0x0000, INV_C_DOWN},
        {"--inv-c-right", 0, 0x0000, INV_C_RIGHT},
        {"--inv-subscreen", 0, 0x0000, INV_SUBSCREEN},
        {"--item-arrows", 0, 0x0000, ITEM_ARROWS},
        {"--item-bombs", 0, 0x0000, ITEM_BOMBS},
        {"--item-bombchu", 0, 0x0000, ITEM_BOMBCHU},
        {"--item-deku-sticks", 0, 0x0000, ITEM_DEKU_STICKS},
        {"--item-deku-nuts", 0, 0x0000, ITEM_DEKU_NUTS},
        {"--item-magic-beans", 0, 0x0000, ITEM_MAGIC_BEANS},
        {"--item-powder-kegs", 0, 0x0000, ITEM_POWDER_KEGS},
        {"--wallet", 0, 0x0000, WALLET},
        {"--heart-assembly", 0, 0x0000, HEART_ASSEMBLY},
        {"--item-pictograph", 0, 0x0000, ITEM_PICTOGRAPH},
        {"--songs", 0, 0x0000, SONGS},
        {"--boss-masks", 0, 0x0000, BOSS_MASKS},
        {"--temple-Woodfall", 0, 0x0000, TEMPLE_WOODFALL},
        {"--temple-Snowhead", 0, 0x0000, TEMPLE_SNOWHEAD},
        {"--temple-Great-Bay", 0, 0x0000, TEMPLE_GREAT_BAY},
        {"--temple-Stone-Tower", 0, 0x0000, TEMPLE_STONE_TOWER},
        {"--small-keys-Woodfall", 0, 0x0000, SMALL_KEYS_WOODFALL},
        {"--small-keys-Snowhead", 0, 0x0000, SMALL_KEYS_SNOWHEAD},
        {"--small-keys-Great-Bay", 0, 0x0000, SMALL_KEYS_GREAT_BAY},
        {"--small-keys-Stone-Tower", 0, 0x0000, SMALL_KEYS_STONE_TOWER},
        {"--stray-fairies-Woodfall", 0, 0x0000, STRAY_FAIRIES_WOODFALL},
        {"--stray-fairies-Snowhead", 0, 0x0000, STRAY_FAIRIES_SNOWHEAD},
        {"--stray-fairies-Great-Bay", 0, 0x0000, STRAY_FAIRIES_GREAT_BAY},
        {"--stray-fairies-Stone-Tower", 0, 0x0000, STRAY_FAIRIES_STONE_TOWER},
        {"--bank", 0, 0x0000, BANK},
        {"--gate", 0, 0x0000, GATE},
        {"--heart-piece", 0, 0x0000, HEART_PIECE}
    };
    return 0;
}

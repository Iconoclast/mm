@ECHO OFF
TITLE MinGW Compiler Suite Invocation
CD C:\MINGW\BIN\

ECHO Compiling to Intel assembly...
GCC.EXE -S -o ../flash/memory.s ../flash/memory.c
ECHO.

ECHO Assembling to program object...
AS.EXE --statistics -o ../flash/memory.o ../flash/memory.s
ECHO.

ECHO Linking to Windows libraries...
GCC.EXE -s -o ../flash/memory.exe ../flash/memory.o
PAUSE

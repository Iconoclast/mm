#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
static char digits[128] = { /* Convert hexadecimal ASCII digits to constants. */
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
     0,  1,  2,  3,  4,  5,  6,  7,  8,  9, -1, -1, -1, -1, -1, -1,
    -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
static unsigned char note[0x0200]; /* note size:  two pages */

void RW0(char *hexadecimal);
void health_num(char *decimal);
void health_den(char *decimal);
void magic_num(char *decimal);
void magic_den(char *decimal);
void agility(char *decimal);
void defense(char *decimal);
void spirit_acc(char *hexadecimal);
void exp_count(char *hexadecimal);
void clipping(char *hexadecimal);
void RW1(char *hexadecimal);
void perspective(char *hexadecimal);
void RW2(char *hexadecimal);
void clipper(char *hexadecimal);
void element_fire(char *decimal);
void element_earth(char *decimal);
void element_water(char *decimal);
void element_wind(char *decimal);
void acc_health(char *hexadecimal);
void acc_magic(char *hexadecimal);
void acc_agility(char *hexadecimal);
void acc_defense(char *hexadecimal);
void RW3(char *hexadecimal);
void RW4(char *hexadecimal);
void RW5(char *hexadecimal);
void RW6(char *hexadecimal);
void time(char *hexadecimal);
void days(char *hexadecimal);
void room(char *hexadecimal);
void spot(char *hexadecimal);
void RW7(char *hexadecimal);
void RW8(char *hexadecimal);
void RW9(char *hexadecimal);
void treasure(char *hexadecimal);
void spirits(char *hexadecimal);
void inventory(char *hexadecimal);

int main(int argc, char *argv[])
{
    FILE *stream;
    if (argc == 1)
    {
        printf("Missing game note information.\n");
        return 1;
    }
    stream = fopen(argv[1], "rb");
    if (stream == NULL)
    {
        printf("Specified file was unreadable.\n");
        return 1;
    }
    fread(note, sizeof(unsigned char), 0x0200, stream);
    fclose(stream);
    if (argc == 2)
    {
        printf("Modifier option not specified.\n");
        return 1;
    }
    if (argv[2][2] == '\0') 
    {
        if ((argv[2][0] & 0xFD) == '-') /* if (str[0] == '-' || str[0] == '/') */
        {
            char block_indices[128] = {
  /* NUL: SI */  0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  /* DLE: US */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  /* ' ':'/' */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  /* '0':'?' */  0, 10, 12, 22, 23, 24, 25, 30, 31, 32, -1, -1, -1, -1, -1, -1,
  /* '@':'O' */ -1, -1, -1, 13, 27,  8, -1, -1,  2, -1, -1, -1, -1,  4, -1, -1,
  /* 'P':'_' */ 11, -1, 28, 29, 26, -1, 14, 16, 18, 19, -1, -1, -1, -1, -1, -1,
  /* '`':'o' */ -1,  5, -1,  9,  6, -1, -1, 34,  1, 35, -1, -1, -1,  3, -1, -1,
  /* 'p':DEL */ -1, -1, -1,  7, 33, -1, 15, 17, 21, 20, -1, -1, -1, -1, -1,  0
            };
            register char func_ptr = block_indices[argv[2][1]];
            void (*branch_table[36])(char(*)) = {
                RW0, /* NUL or 0xFF */
                health_num, /* 'h' */
                health_den, /* 'H' */
                magic_num, /* 'm' */
                magic_den, /* 'M' */
                agility, /* 'a' */
                defense, /* 'd' */
                spirit_acc, /* 's' */
                exp_count, /* 'E' */
                clipping, /* 'c' */
                RW1, /* '1' */
                perspective, /* 'P' */
                RW2, /* '2' */
                clipper, /* 'C' */
                element_fire, /* 'V' */
                element_earth, /* 'v' */
                element_water, /* 'W' */
                element_wind, /* 'w' */
                acc_health, /* 'X' */
                acc_magic, /* 'Y' */
                acc_agility, /* 'y' */
                acc_defense, /* 'x' */
                RW3, /* '3' */
                RW4, /* '4' */
                RW5, /* '5' */
                RW6, /* '6' */
                time, /* 'T' */
                days, /* 'D' */
                room, /* 'R' */
                spot, /* 'S' */
                RW7, /* '7' */
                RW8, /* '8' */
                RW9, /* '9' */
                treasure, /* 't' */
                spirits, /* 'g' */
                inventory /* 'i' */
            };
            if (func_ptr == -1)
            {
                printf("Invalid option switch implied.\n");
                return 1;
            }
            branch_table[func_ptr](argv[3]);
            stream = fopen(argv[1], "wb");
            fwrite(note, sizeof(unsigned char), 0x0200, stream);
            fclose(stream);
            return 0;
        }
        printf("Bad data in the option string.\n");
        return 1;
    }
    printf("Invalid option switch implied.\n");
    return 1;
}

void RW0(char *hexadecimal)
{
    note[0x0000]  = digits[hexadecimal[0]] << 4;
    note[0x0000] |= digits[hexadecimal[1]];
    note[0x0001]  = digits[hexadecimal[2]] << 4;
    note[0x0001] |= digits[hexadecimal[3]];
    note[0x0002]  = digits[hexadecimal[4]] << 4;
    note[0x0002] |= digits[hexadecimal[5]];
    note[0x0003]  = digits[hexadecimal[6]] << 4;
    note[0x0003] |= digits[hexadecimal[7]];
}
void health_num(char *decimal)
{
    register unsigned int health = 0x0000;
    register unsigned int i = 0x0000;
    do
    {
        health *= 10;
        health += decimal[i] ^ 0x30;
        ++i;
    } while (!(decimal[i] == '\0'));
    note[0x0004] = (unsigned char)(health >> 8);
    note[0x0005] = health & 0x00FF;
}
void health_den(char *decimal)
{
    register unsigned int health = 0x0000;
    register unsigned int i = 0x0000;
    do
    {
        health *= 10;
        health += decimal[i] ^ 0x30;
        ++i;
    } while (!(decimal[i] == '\0'));
    note[0x0006] = (unsigned char)(health >> 8);
    note[0x0007] = health & 0x00FF;
}
void magic_num(char *decimal)
{
    register unsigned int magic = 0x0000;
    register unsigned int i = 0x0000;
    do
    {
        magic *= 10;
        magic += decimal[i] ^ 0x30;
        ++i;
    } while (!(decimal[i] == '\0'));
    note[0x0008] = (unsigned char)(magic >> 8);
    note[0x0009] = magic & 0x00FF;
}
void magic_den(char *decimal)
{
    register unsigned int magic = 0x0000;
    register unsigned int i = 0x0000;
    do
    {
        magic *= 10;
        magic += decimal[i] ^ 0x30;
        ++i;
    } while (!(decimal[i] == '\0'));
    note[0x000A] = (unsigned char)(magic >> 8);
    note[0x000B] = magic & 0x00FF;
}
void agility(char *decimal)
{
    register unsigned int agility = 0x0000;
    register unsigned int i = 0x0000;
    do
    {
        agility *= 10;
        agility += decimal[i] ^ 0x30;
        ++i;
    } while (!(decimal[i] == '\0'));
    note[0x000C] = (unsigned char)(agility >> 8);
    note[0x000D] = agility & 0x00FF;
}
void defense(char *decimal)
{
    register unsigned int defense = 0x0000;
    register unsigned int i = 0x0000;
    do
    {
        defense *= 10;
        defense += decimal[i] ^ 0x30;
        ++i;
    } while (!(decimal[i] == '\0'));
    note[0x000E] = (unsigned char)(defense >> 8);
    note[0x000F] = defense & 0x00FF;
}
void spirit_acc(char *hexadecimal)
{
    note[0x0010]  = digits[hexadecimal[0]] << 4;
    note[0x0010] |= digits[hexadecimal[1]];
    note[0x0011]  = digits[hexadecimal[2]] << 4;
    note[0x0011] |= digits[hexadecimal[3]];
    note[0x0012]  = digits[hexadecimal[4]] << 4;
    note[0x0012] |= digits[hexadecimal[5]];
    note[0x0013]  = digits[hexadecimal[6]] << 4;
    note[0x0013] |= digits[hexadecimal[7]];
}
void exp_count(char *hexadecimal)
{
    note[0x0014]  = digits[hexadecimal[0]] << 4;
    note[0x0014] |= digits[hexadecimal[1]];
    note[0x0015]  = digits[hexadecimal[2]] << 4;
    note[0x0015] |= digits[hexadecimal[3]];
    note[0x0016]  = digits[hexadecimal[4]] << 4;
    note[0x0016] |= digits[hexadecimal[5]];
    note[0x0017]  = digits[hexadecimal[6]] << 4;
    note[0x0017] |= digits[hexadecimal[7]];
}
void clipping(char *hexadecimal)
{
    note[0x0018]  = digits[hexadecimal[0]] << 4;
    note[0x0018] |= digits[hexadecimal[1]];
    note[0x0019]  = digits[hexadecimal[2]] << 4;
    note[0x0019] |= digits[hexadecimal[3]];
}
void RW1(char *hexadecimal)
{
    note[0x001A]  = digits[hexadecimal[0]] << 4;
    note[0x001A] |= digits[hexadecimal[1]];
    note[0x001B]  = digits[hexadecimal[2]] << 4;
    note[0x001B] |= digits[hexadecimal[3]];
}
void perspective(char *hexadecimal)
{
    note[0x001C]  = digits[hexadecimal[0]] << 4;
    note[0x001C] |= digits[hexadecimal[1]];
    note[0x001D]  = digits[hexadecimal[2]] << 4;
    note[0x001D] |= digits[hexadecimal[3]];
}
void RW2(char *hexadecimal)
{
    note[0x001E]  = digits[hexadecimal[0]] << 4;
    note[0x001E] |= digits[hexadecimal[1]];
    note[0x001F]  = digits[hexadecimal[2]] << 4;
    note[0x001F] |= digits[hexadecimal[3]];
}
void clipper(char *hexadecimal)
{
    note[0x0020]  = digits[hexadecimal[0]] << 4;
    note[0x0020] |= digits[hexadecimal[1]];
    note[0x0021]  = digits[hexadecimal[2]] << 4;
    note[0x0021] |= digits[hexadecimal[3]];
    note[0x0022]  = digits[hexadecimal[4]] << 4;
    note[0x0022] |= digits[hexadecimal[5]];
    note[0x0023]  = digits[hexadecimal[6]] << 4;
    note[0x0023] |= digits[hexadecimal[7]];
}
void element_fire(char *decimal)
{
    register unsigned char spirits = 0x0000;
    register unsigned int i = 0x0000;
    do
    {
        spirits *= 10;
        spirits += decimal[i] ^ 0x30;
        ++i;
    } while (!(decimal[i] == '\0'));
    note[0x0024] = spirits & 0x00FF;
}
void element_earth(char *decimal)
{
    register unsigned char spirits = 0x0000;
    register unsigned int i = 0x0000;
    do
    {
        spirits *= 10;
        spirits += decimal[i] ^ 0x30;
        ++i;
    } while (!(decimal[i] == '\0'));
    note[0x0025] = spirits & 0x00FF;
}
void element_water(char *decimal)
{
    register unsigned char spirits = 0x0000;
    register unsigned int i = 0x0000;
    do
    {
        spirits *= 10;
        spirits += decimal[i] ^ 0x30;
        ++i;
    } while (!(decimal[i] == '\0'));
    note[0x0026] = spirits & 0x00FF;
}
void element_wind(char *decimal)
{
    register unsigned char spirits = 0x0000;
    register unsigned int i = 0x0000;
    do
    {
        spirits *= 10;
        spirits += decimal[i] ^ 0x30;
        ++i;
    } while (!(decimal[i] == '\0'));
    note[0x0027] = spirits & 0x00FF;
}
void acc_health(char *hexadecimal)
{
    note[0x0028]  = digits[hexadecimal[0]] << 4;
    note[0x0028] |= digits[hexadecimal[1]];
    note[0x0029]  = digits[hexadecimal[2]] << 4;
    note[0x0029] |= digits[hexadecimal[3]];
}
void acc_magic(char *hexadecimal)
{
    note[0x002A]  = digits[hexadecimal[0]] << 4;
    note[0x002A] |= digits[hexadecimal[1]];
    note[0x002B]  = digits[hexadecimal[2]] << 4;
    note[0x002B] |= digits[hexadecimal[3]];
}
void acc_agility(char *hexadecimal)
{
    note[0x002C]  = digits[hexadecimal[0]] << 4;
    note[0x002C] |= digits[hexadecimal[1]];
    note[0x002D]  = digits[hexadecimal[2]] << 4;
    note[0x002D] |= digits[hexadecimal[3]];
}
void acc_defense(char *hexadecimal)
{
    note[0x002E]  = digits[hexadecimal[0]] << 4;
    note[0x002E] |= digits[hexadecimal[1]];
    note[0x002F]  = digits[hexadecimal[2]] << 4;
    note[0x002F] |= digits[hexadecimal[3]];
}
void RW3(char *hexadecimal)
{
    note[0x0030]  = digits[hexadecimal[0]] << 4;
    note[0x0030] |= digits[hexadecimal[1]];
    note[0x0031]  = digits[hexadecimal[2]] << 4;
    note[0x0031] |= digits[hexadecimal[3]];
}
void RW4(char *hexadecimal)
{
    note[0x0032]  = digits[hexadecimal[0]] << 4;
    note[0x0032] |= digits[hexadecimal[1]];
    note[0x0033]  = digits[hexadecimal[2]] << 4;
    note[0x0033] |= digits[hexadecimal[3]];
}
void RW5(char *hexadecimal)
{
    note[0x0034]  = digits[hexadecimal[0]] << 4;
    note[0x0034] |= digits[hexadecimal[1]];
    note[0x0035]  = digits[hexadecimal[2]] << 4;
    note[0x0035] |= digits[hexadecimal[3]];
}
void RW6(char *hexadecimal)
{
    note[0x0036]  = digits[hexadecimal[0]] << 4;
    note[0x0036] |= digits[hexadecimal[1]];
    note[0x0037]  = digits[hexadecimal[2]] << 4;
    note[0x0037] |= digits[hexadecimal[3]];
}
void time(char *hexadecimal)
{
    note[0x0038]  = digits[hexadecimal[0]] << 4;
    note[0x0038] |= digits[hexadecimal[1]];
    note[0x0039]  = digits[hexadecimal[2]] << 4;
    note[0x0039] |= digits[hexadecimal[3]];
}
void days(char *hexadecimal)
{
    note[0x003A]  = digits[hexadecimal[0]] << 4;
    note[0x003A] |= digits[hexadecimal[1]];
    note[0x003B]  = digits[hexadecimal[2]] << 4;
    note[0x003B] |= digits[hexadecimal[3]];
}
void room(char *hexadecimal)
{
    note[0x003C]  = digits[hexadecimal[0]] << 4;
    note[0x003C] |= digits[hexadecimal[1]];
    note[0x003D]  = digits[hexadecimal[2]] << 4;
    note[0x003D] |= digits[hexadecimal[3]];
    note[0x003E]  = digits[hexadecimal[4]] << 4;
    note[0x003E] |= digits[hexadecimal[5]];
    note[0x003F]  = digits[hexadecimal[6]] << 4;
    note[0x003F] |= digits[hexadecimal[7]];
}
void spot(char *hexadecimal)
{
    note[0x0040]  = digits[hexadecimal[0]] << 4;
    note[0x0040] |= digits[hexadecimal[1]];
    note[0x0041]  = digits[hexadecimal[2]] << 4;
    note[0x0041] |= digits[hexadecimal[3]];
    note[0x0042]  = digits[hexadecimal[4]] << 4;
    note[0x0042] |= digits[hexadecimal[5]];
    note[0x0043]  = digits[hexadecimal[6]] << 4;
    note[0x0043] |= digits[hexadecimal[7]];
}
void RW7(char *hexadecimal)
{
    note[0x0044]  = digits[hexadecimal[0]] << 4;
    note[0x0044] |= digits[hexadecimal[1]];
    note[0x0045]  = digits[hexadecimal[2]] << 4;
    note[0x0045] |= digits[hexadecimal[3]];
    note[0x0046]  = digits[hexadecimal[4]] << 4;
    note[0x0046] |= digits[hexadecimal[5]];
    note[0x0047]  = digits[hexadecimal[6]] << 4;
    note[0x0047] |= digits[hexadecimal[7]];
}
void RW8(char *hexadecimal)
{
    note[0x0048]  = digits[hexadecimal[0]] << 4;
    note[0x0048] |= digits[hexadecimal[1]];
    note[0x0049]  = digits[hexadecimal[2]] << 4;
    note[0x0049] |= digits[hexadecimal[3]];
    note[0x004A]  = digits[hexadecimal[4]] << 4;
    note[0x004A] |= digits[hexadecimal[5]];
    note[0x004B]  = digits[hexadecimal[6]] << 4;
    note[0x004B] |= digits[hexadecimal[7]];
}
void RW9(char *hexadecimal)
{
    note[0x004C]  = digits[hexadecimal[0]] << 4;
    note[0x004C] |= digits[hexadecimal[1]];
    note[0x004D]  = digits[hexadecimal[2]] << 4;
    note[0x004D] |= digits[hexadecimal[3]];
    note[0x004E]  = digits[hexadecimal[4]] << 4;
    note[0x004E] |= digits[hexadecimal[5]];
    note[0x004F]  = digits[hexadecimal[6]] << 4;
    note[0x004F] |= digits[hexadecimal[7]];
}
void treasure(char *hexadecimal)
{
    note[0x0080]  = digits[hexadecimal [0]] << 4;
    note[0x0080] |= digits[hexadecimal [1]];
    note[0x0081]  = digits[hexadecimal [2]] << 4;
    note[0x0081] |= digits[hexadecimal [3]];
    note[0x0082]  = digits[hexadecimal [4]] << 4;
    note[0x0082] |= digits[hexadecimal [5]];
    note[0x0083]  = digits[hexadecimal [6]] << 4;
    note[0x0083] |= digits[hexadecimal [7]];
    note[0x0084]  = digits[hexadecimal [8]] << 4;
    note[0x0084] |= digits[hexadecimal [9]];
    note[0x0085]  = digits[hexadecimal[10]] << 4;
    note[0x0085] |= digits[hexadecimal[11]];
    note[0x0086]  = digits[hexadecimal[12]] << 4;
    note[0x0086] |= digits[hexadecimal[13]];
    note[0x0087]  = digits[hexadecimal[14]] << 4;
    note[0x0087] |= digits[hexadecimal[15]];
    note[0x0088]  = digits[hexadecimal[16]] << 4;
    note[0x0088] |= digits[hexadecimal[17]];
    note[0x0089]  = digits[hexadecimal[18]] << 4;
    note[0x0089] |= digits[hexadecimal[19]];
    note[0x008A]  = digits[hexadecimal[20]] << 4;
    note[0x008A] |= digits[hexadecimal[21]];
    note[0x008B]  = digits[hexadecimal[22]] << 4;
    note[0x008B] |= digits[hexadecimal[23]];
    note[0x008C]  = digits[hexadecimal[24]] << 4;
    note[0x008C] |= digits[hexadecimal[25]];
    note[0x008D]  = digits[hexadecimal[26]] << 4;
    note[0x008D] |= digits[hexadecimal[27]];
    note[0x008E]  = digits[hexadecimal[28]] << 4;
    note[0x008E] |= digits[hexadecimal[29]];
    note[0x008F]  = digits[hexadecimal[30]] << 4;
    note[0x008F] |= digits[hexadecimal[31]];
}
void spirits(char *hexadecimal)
{
    note[0x00A0]  = digits[hexadecimal [0]] << 4;
    note[0x00A0] |= digits[hexadecimal [1]];
    note[0x00A1]  = digits[hexadecimal [2]] << 4;
    note[0x00A1] |= digits[hexadecimal [3]];
    note[0x00A2]  = digits[hexadecimal [4]] << 4;
    note[0x00A2] |= digits[hexadecimal [5]];
    note[0x00A3]  = digits[hexadecimal [6]] << 4;
    note[0x00A3] |= digits[hexadecimal [7]];
    note[0x00A4]  = digits[hexadecimal [8]] << 4;
    note[0x00A4] |= digits[hexadecimal [9]];
    note[0x00A5]  = digits[hexadecimal[10]] << 4;
    note[0x00A5] |= digits[hexadecimal[11]];
    note[0x00A6]  = digits[hexadecimal[12]] << 4;
    note[0x00A6] |= digits[hexadecimal[13]];
    note[0x00A7]  = digits[hexadecimal[14]] << 4;
    note[0x00A7] |= digits[hexadecimal[15]];
    note[0x00A8]  = digits[hexadecimal[16]] << 4;
    note[0x00A8] |= digits[hexadecimal[17]];
    note[0x00A9]  = digits[hexadecimal[18]] << 4;
    note[0x00A9] |= digits[hexadecimal[19]];
    note[0x00AA]  = digits[hexadecimal[20]] << 4;
    note[0x00AA] |= digits[hexadecimal[21]];
    note[0x00AB]  = digits[hexadecimal[22]] << 4;
    note[0x00AB] |= digits[hexadecimal[23]];
    note[0x00AC]  = digits[hexadecimal[24]] << 4;
    note[0x00AC] |= digits[hexadecimal[25]];
    note[0x00AD]  = digits[hexadecimal[26]] << 4;
    note[0x00AD] |= digits[hexadecimal[27]];
    note[0x00AE]  = digits[hexadecimal[28]] << 4;
    note[0x00AE] |= digits[hexadecimal[29]];
    note[0x00AF]  = digits[hexadecimal[30]] << 4;
    note[0x00AF] |= digits[hexadecimal[31]];
}
void inventory(char *hexadecimal)
{
    register unsigned int offset = digits[hexadecimal[0]] << 4;
    offset = offset | digits[hexadecimal[1]];
    offset = offset % 0x96; /* Prevent memory access overrun. */
    offset = 0x00B0 | offset;
    note[offset]  = digits[hexadecimal[2]] << 4;
    note[offset] |= digits[hexadecimal[3]];
}

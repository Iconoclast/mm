@ECHO OFF
TITLE Controller Pak Automated Update
memory cont_ram.bin -w 0
et "QUEST64-0.bin" -h 999
et "QUEST64-0.bin" -H 999
et "QUEST64-0.bin" -m 999
et "QUEST64-0.bin" -M 999
et "QUEST64-0.bin" -a 65535
et "QUEST64-0.bin" -d 255
et "QUEST64-0.bin" -V 50
et "QUEST64-0.bin" -v 50
et "QUEST64-0.bin" -W 50
et "QUEST64-0.bin" -w 50
et "QUEST64-0.bin" -T 7FFF
et "QUEST64-0.bin" -D 18
et "QUEST64-0.bin" -R 00000017
et "QUEST64-0.bin" -S 00000016
et "QUEST64-0.bin" -t FFFFFFFFFFFFFFFFFFFFFF0000000000
et "QUEST64-0.bin" -g FFFFFFFFFFFFFFFFFFFFFFFF03000000
et "QUEST64-0.bin" -i 0014
et "QUEST64-0.bin" -i 0115
et "QUEST64-0.bin" -i 0216
et "QUEST64-0.bin" -i 0317
et "QUEST64-0.bin" -i 0418
et "QUEST64-0.bin" -i 0519
et "QUEST64-0.bin" -i 060E
et "QUEST64-0.bin" -i 070F
et "QUEST64-0.bin" -i 0810
et "QUEST64-0.bin" -i 0911
et "QUEST64-0.bin" -i 0A12
et "QUEST64-0.bin" -i 0B13
memory cont_ram.bin -o 0
COPY cont_ram.bin C:\USERS\RJ\EMULAT~1\N64\SAVE\QUEST6~1.MPK
PAUSE

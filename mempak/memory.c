#define MEM_SIZE    0x8000
#define _CRT_SECURE_NO_WARNINGS
#include    <memory.h>
#include    <stdio.h>

char digits[128] = { /* Convert hexadecimal ASCII digits to constants. */
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
     0,  1,  2,  3,  4,  5,  6,  7,  8,  9, -1, -1, -1, -1, -1, -1,
    -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
unsigned char memory[0x8000];

void format(char *Boolean);
void help(char *Boolean);
void note_erase(char *integer);
void note_export(char *integer);
void note_overwrite(char *integer);
void note_rename(char *string);
void page_export(char *integer);

int main(int argc, char *argv[])
{
    FILE *stream;
    /* security checking:  argument two, the controller RAM filepath */
    if (argc == 1)
    {
        printf("Controller Pak file undefined.\n");
        return 1;
    }
    stream = fopen(argv[1], "rb");
    if (stream == NULL)
    {
        printf("Specified file was unreadable.\n");
        return 1;
    }
    fread(memory, sizeof(unsigned char), 0x8000, stream);
    if (memory[0x0000] != 0x81)
    {
        printf("Controller Pak array mismatch.\n");
        return 1;
    }
    fclose(stream);

    /* security checking:  argument three, user requests */
    if (argc == 2)
    {
        help(0);
        return 1;
    }
    if ((argv[2][0] & 0xFD) == '-') /* if (str[0] == '-' || str[0] == '/') */
    {
        char block_indices[128] = {
             0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
             0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  0,
            -1, -1, -1, -1, -1,  4,  1, -1,  0,  1, -1, -1, -1, -1, -1,  5,
             2, -1,  6, -1, -1, -1, -1,  3, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1,  4,  1, -1,  0,  1, -1, -1, -1, -1, -1,  5,
             2, -1,  6, -1, -1, -1, -1,  3, -1, -1, -1, -1, -1, -1, -1, -1 };
        void (*branch_table[8])(char(*)) = {
            help,
            format,
            page_export,
            note_export,
            note_erase,
            note_overwrite,
            note_rename
        };
        if (block_indices[argv[2][1]] == -1)
        {
            help(0);
            return 1;
        }
        branch_table[block_indices[argv[2][1]]](argv[3]);
        return 0;
    }
    else
    {
        help(0);
        return 1;
    }
}

void format(char *Boolean)
{
    FILE *stream;
    if (Boolean == 0)
    {
        /* bah, stupid pages 1 and 2...need to fill all that up I guess */
        /* to-do ... */
        memset(memory + 0x0300, 0x00, 0x0200);
        memset(memory + 0x0500, 0xFF, 0x7B00);
        stream = fopen("cont_ram.bin", "wb");
        fwrite(memory, sizeof(unsigned char), 0x8000, stream);
        fclose(stream);
        return;
    }
    memory[0x0000] = 0x81;
    memory[0x0023] = memory[0x0022] = memory[0x0021] = memory[0x0020] =
    memory[0x001F] = memory[0x001E] = memory[0x001D] = memory[0x001C] =
    memory[0x001B] = memory[0x001A] = memory[0x0019] = memory[0x0018] =
    memory[0x0017] = memory[0x0016] = memory[0x0015] = memory[0x0014] =
    memory[0x0013] = memory[0x0012] = memory[0x0011] = memory[0x0010] =
    memory[0x000F] = memory[0x000E] = memory[0x000D] = memory[0x000C] =
    memory[0x000B] = memory[0x000A] = memory[0x0009] = memory[0x0008] =
    memory[0x0007] = memory[0x0006] = memory[0x0005] = memory[0x0004] =
    memory[0x0003] = memory[0x0002] = memory[0x0001] = 0xFF;
    memory[0x0027] = memory[0x0026] = memory[0x0025] = memory[0x0024] = 0x00;
    memory[0x0039] = memory[0x0038] = memory[0x0037] = memory[0x0036] =
    memory[0x0035] = memory[0x0034] = memory[0x0033] = memory[0x0032] =
    memory[0x0031] = memory[0x0030] = memory[0x002F] = memory[0x002E] =
    memory[0x002D] = memory[0x002C] = memory[0x002B] = memory[0x002A] =
    memory[0x0029] = memory[0x0028] = memory[0x0027] = memory[0x0026] =
    memory[0x0025] = memory[0x0024] = 0xFF;
    memory[0x003A] = 0x01;
    memory[0x003B] = 0xFF;
    memory[0x003F] = memory[0x003E] = memory[0x003D] = memory[0x003C] = 0x00;
    memory[0x0063] = memory[0x0062] = memory[0x0061] = memory[0x0060] =
    memory[0x005F] = memory[0x005E] = memory[0x005D] = memory[0x005C] =
    memory[0x005B] = memory[0x005A] = memory[0x0059] = memory[0x0058] =
    memory[0x0057] = memory[0x0056] = memory[0x0055] = memory[0x0054] =
    memory[0x0053] = memory[0x0052] = memory[0x0051] = memory[0x0050] =
    memory[0x004F] = memory[0x004E] = memory[0x004D] = memory[0x004C] =
    memory[0x004B] = memory[0x004A] = memory[0x0049] = memory[0x0048] =
    memory[0x0047] = memory[0x0046] = memory[0x0045] = memory[0x0044] =
    memory[0x0043] = memory[0x0042] = memory[0x0041] = memory[0x0040] = 0xFF;
    memory[0x0067] = memory[0x0066] = memory[0x0065] = memory[0x0064] = 0x00;
    memory[0x0079] = memory[0x0078] = memory[0x0077] = memory[0x0076] =
    memory[0x0075] = memory[0x0074] = memory[0x0073] = memory[0x0072] =
    memory[0x0071] = memory[0x0070] = memory[0x006F] = memory[0x006E] =
    memory[0x006D] = memory[0x006C] = memory[0x006B] = memory[0x006A] =
    memory[0x0069] = memory[0x0068] = 0xFF;
    memory[0x007A] = 0x01;
    memory[0x007B] = 0xFF;
    memory[0x007F] = memory[0x007E] = memory[0x007D] = memory[0x007C] = 0x00;
    memory[0x0083] = memory[0x0082] = memory[0x0081] = memory[0x0080] = 0xFF;
    memory[0x0087] = memory[0x0086] = memory[0x0085] = memory[0x0084] = 0x00;
    memory[0x0099] = memory[0x0098] = memory[0x0097] = memory[0x0096] =
    memory[0x0095] = memory[0x0094] = memory[0x0093] = memory[0x0092] =
    memory[0x0091] = memory[0x0090] = memory[0x0089] = memory[0x0088] = 0xFF;
    memory[0x009A] = 0x01;
    memory[0x009B] = 0xFF;
    memory[0x009F] = memory[0x009E] = memory[0x009D] = memory[0x009C] = 0x00;
    memory[0x00C3] = memory[0x00C2] = memory[0x00C1] = memory[0x00C0] =
    memory[0x00BF] = memory[0x00BE] = memory[0x00BD] = memory[0x00BC] =
    memory[0x00BB] = memory[0x00BA] = memory[0x00B9] = memory[0x00B8] =
    memory[0x00B7] = memory[0x00B6] = memory[0x00B5] = memory[0x00B4] =
    memory[0x00B3] = memory[0x00B2] = memory[0x00B1] = memory[0x00B0] =
    memory[0x00AF] = memory[0x00AE] = memory[0x00AD] = memory[0x00AC] =
    memory[0x00AB] = memory[0x00AA] = memory[0x00A9] = memory[0x00A8] =
    memory[0x00A7] = memory[0x00A6] = memory[0x00A5] = memory[0x00A4] =
    memory[0x00A3] = memory[0x00A2] = memory[0x00A1] = memory[0x00A0] = 0xFF;
    memory[0x00C7] = memory[0x00C6] = memory[0x00C5] = memory[0x00C4] = 0x00;
    memory[0x00D9] = memory[0x00D8] = memory[0x00D7] = memory[0x00D6] =
    memory[0x00D5] = memory[0x00D4] = memory[0x00D3] = memory[0x00D2] =
    memory[0x00D1] = memory[0x00D0] = memory[0x00C9] = memory[0x00C8] = 0xFF;
    memory[0x00DA] = 0x01;
    memory[0x00DB] = 0xFF;
    memory[0x00DF] = memory[0x00DE] = memory[0x00DD] = memory[0x00DC] = 0x00;
    memory[0x00FF] = memory[0x00FE] = memory[0x00FD] = memory[0x00FC] =
    memory[0x00FB] = memory[0x00FA] = memory[0x00F9] = memory[0x00F8] =
    memory[0x00F7] = memory[0x00F6] = memory[0x00F5] = memory[0x00F4] =
    memory[0x00F3] = memory[0x00F2] = memory[0x00F1] = memory[0x00F0] =
    memory[0x00EF] = memory[0x00EE] = memory[0x00ED] = memory[0x00EC] =
    memory[0x00EB] = memory[0x00EA] = memory[0x00E9] = memory[0x00E8] =
    memory[0x00E7] = memory[0x00E6] = memory[0x00E5] = memory[0x00E4] =
    memory[0x00E3] = memory[0x00E2] = memory[0x00E1] = memory[0x00E0] = 0xFF;
    stream = fopen("cont_ram.bin", "wb");
    fwrite(memory, sizeof(unsigned char), 0x8000, stream);
    fclose(stream);
    return;
}

void help(char *Boolean)
{
    if (Boolean == 0)
    {
        printf("Command syntax missing action.\n");
    }
    printf("memory [source] [option] [operand]\n"\
           "[source]:  File path to a Nintendo 64 controller RAM file dump.\n"\
           "[option]:  '-' or '/' followed by one ASCII character:\n"\
           "   E:  Erase note from Controller Pak file.\n"\
           "   F:  Format Controller Pak file.\n"\
           "   H:  Display program command syntax.\n"\
           "   O:  Overwrite Controller Pak note with external note binary.\n"\
           "   P:  Export one page of controller RAM to page_###.bin.\n"\
           "   R:  Rename game note label.\n"\
           "   W:  Export note from Controller Pak file.\n"\
           "[operand]:  Given [option]([operand]).  See README for details.\n");
    return;
}

void note_erase(char *integer)
{
    FILE *stream;
    register unsigned int offset = 0x0300 + (digits[integer[0]] << 5);
    register unsigned int entry; /* entry page into note */

    memory[offset++] = 0x00; /* header[0x00] ... */
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    entry = memory[offset] << 8; /* header[0x06]:  high-order byte of int */
    memory[offset++] = 0x00;
    entry = entry | memory[offset]; /* header[0x07]:  low-order byte of int */
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset++] = 0x00;
    memory[offset]   = 0x00; /* ... header[0x1F] */

    if (!(entry >> 7 == 0)) /* if (entry > 0x007F) */
    {
        printf("Out-of-bounds.\n");
        return;
    }
    do
    {
        offset = 0x0101 | (entry << 2); /* target pointer to page node index */
        entry = memory[offset];
        memset(memory + (digits[integer[0]] << 8), 256, 0x00);
        memory[offset] = 0x03; /* free page */
    } while (!(entry == 0x0001));
    stream = fopen("cont_ram.bin", "wb");
    fwrite(memory, sizeof(unsigned char), 0x8000, stream);
}

void note_export(char *integer)
{
    FILE *stream;
    char code_page_Nintendo[256] = {
    /*   +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F  */
        000,127,127,127,127,127,127,127,127,127,127,127,127,127,127,' ',
        '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F',
        'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V',
        'W','X','Y','Z','!','_','#',047,'_','+',',','-','.','_','_','=',
        '_','@','`','`','`','`','`','`','`','`','`','`','`','`','`','`',
        '`','`','`','`','`','`','`','`','`','`','`','`','`','`','`','`',
        '`','`','`','`','`','`','`','`','`','`','`','`','`','`','`','`',
        '`','`','`','`','`','`','`','`','`','`','`','`','`','`','`','`',
        '`','`','`','`','`','`','`','`','`','`','`','`','`','`','`','`',
        '`','`','`','`','`',127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127 };
    char filename[24] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
    unsigned char note[0x8000];
    register unsigned int base = 0x0000;
    register unsigned int offset; /* base address for memory[] page transfer */

    offset = digits[integer[0]];
    if (offset == 0xFFFF)
    {
        printf("Invalid note.\n");
        return;
    }
    offset = 0x0310 + (offset << 5); /* offset = 0x0310 + (note * 0x0020); */
    filename[ 0] = code_page_Nintendo[memory[offset++]];
    filename[ 1] = code_page_Nintendo[memory[offset++]];
    filename[ 2] = code_page_Nintendo[memory[offset++]];
    filename[ 3] = code_page_Nintendo[memory[offset++]];
    filename[ 4] = code_page_Nintendo[memory[offset++]];
    filename[ 5] = code_page_Nintendo[memory[offset++]];
    filename[ 6] = code_page_Nintendo[memory[offset++]];
    filename[ 7] = code_page_Nintendo[memory[offset++]];
    filename[ 8] = code_page_Nintendo[memory[offset++]];
    filename[ 9] = code_page_Nintendo[memory[offset++]];
    filename[10] = code_page_Nintendo[memory[offset++]];
    filename[11] = code_page_Nintendo[memory[offset++]];
    filename[12] = code_page_Nintendo[memory[offset++]];
    filename[13] = code_page_Nintendo[memory[offset++]];
    filename[14] = code_page_Nintendo[memory[offset++]];
    filename[15] = code_page_Nintendo[memory[offset]];
    offset = offset ^ 0x0018; /* offset -= 0x0018; */
    offset = memory[offset] << 8;
    if (offset >> 15 == 1) /* if (offset > 0x7FFF) */
    {
        printf("Out-of-bounds.\n", offset);
        return;
    }
    do
    {
        register unsigned i = memory[0x0101 | (offset >> 7)];
        do
        {
            note[base++] = memory[offset++];
        } while (!((base & 0x00FF) == 0x0000));
        if (i == 0x0001) break; /* terminal page of note */
        if (i < 0x0005)
        {
            printf("Access fault.\n");
            return;
        }
        offset = i << 8;
    } while (offset == offset);
    for (offset = 0x0000; offset == offset; ++offset)
    { /* Meh, for() loops ... who needs 'em .... */
        if (filename[offset] == '\0') /* ASCII-terminating null byte */
        {
            register unsigned i = memory[0x030C + ((integer[0] & 0x0F) << 5)];
            if (!(i == '\0'))
            { /* If a note extension is specified in the Controller Pak ... */
                filename[offset++] = '-';
                filename[offset++] = code_page_Nintendo[i];
            }
            filename[offset++] = '.';
            filename[offset++] = 'b';
            filename[offset++] = 'i';
            filename[offset] = 'n';
            break;
        }
    } /* This loop is never permanent because there will be a null byte. */
    stream = fopen(filename, "wb");
    fwrite(note, sizeof(unsigned char), base, stream);
    fclose(stream);
    return;
}

void note_overwrite(char *integer)
{
    FILE *stream;
    char code_page_Nintendo[256] = {
    /*   +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F  */
        000,127,127,127,127,127,127,127,127,127,127,127,127,127,127,' ',
        '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F',
        'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V',
        'W','X','Y','Z','!','_','#',047,'_','+',',','-','.','_','_','=',
        '_','@','`','`','`','`','`','`','`','`','`','`','`','`','`','`',
        '`','`','`','`','`','`','`','`','`','`','`','`','`','`','`','`',
        '`','`','`','`','`','`','`','`','`','`','`','`','`','`','`','`',
        '`','`','`','`','`','`','`','`','`','`','`','`','`','`','`','`',
        '`','`','`','`','`','`','`','`','`','`','`','`','`','`','`','`',
        '`','`','`','`','`',127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
        127,127,127,127,127,127,127,127,127,127,127,127,127,127,127,127 };
    char filename[24] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
    unsigned char note[0x8000];
    register unsigned int i = 0x0000;
    register unsigned int offset = 0x0310 + (digits[integer[0]] << 5);
    filename[ 0] = code_page_Nintendo[memory[offset++]];
    filename[ 1] = code_page_Nintendo[memory[offset++]];
    filename[ 2] = code_page_Nintendo[memory[offset++]];
    filename[ 3] = code_page_Nintendo[memory[offset++]];
    filename[ 4] = code_page_Nintendo[memory[offset++]];
    filename[ 5] = code_page_Nintendo[memory[offset++]];
    filename[ 6] = code_page_Nintendo[memory[offset++]];
    filename[ 7] = code_page_Nintendo[memory[offset++]];
    filename[ 8] = code_page_Nintendo[memory[offset++]];
    filename[ 9] = code_page_Nintendo[memory[offset++]];
    filename[10] = code_page_Nintendo[memory[offset++]];
    filename[11] = code_page_Nintendo[memory[offset++]];
    filename[12] = code_page_Nintendo[memory[offset++]];
    filename[13] = code_page_Nintendo[memory[offset++]];
    filename[14] = code_page_Nintendo[memory[offset++]];
    filename[15] = code_page_Nintendo[memory[offset]];
    offset = offset ^ 0x0013; /* offset -= 0x0014; */
    while (filename[i] != '\0') ++i; /* seek string ptr. to null-terminator */
    if (!(memory[offset] == 0x00)) /* note extension character defined */
    {
        filename[i++] = '-';
        filename[i++] = code_page_Nintendo[memory[offset]];
    }
    filename[i++] = '.';
    filename[i++] = 'b';
    filename[i++] = 'i';
    filename[i] = 'n';
    stream = fopen(filename, "rb");
    if (stream == NULL)
    {
        printf("Missing expected file:  \"%s\".\n", filename);
        return;
    }
    fseek(stream, 0, SEEK_END);
    i = ftell(stream); /* return file size in bytes of "filename" to i */
    rewind(stream);
    fread(note, sizeof(unsigned char), i, stream);
    fclose(stream);
    if ((i & 0x00FF) == 0x0000)
    {
        register unsigned int count = 0x0000;
        i = i >> 8; /* file_size_in_bytes / 256 == number_of_pages_in_note */
        offset = offset ^ 0x000B; /* offset -= 0x0005; */
        offset = memory[offset];
        do
        {
            register unsigned int node = offset << 1;
            offset = node << 7; /* offset = offset << 8; */
            node = 0x0101 | node; /* node += 0x0101; // LO bit of node != 1 */
            node = memory[node];
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0000] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0001] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0002] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0003] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0004] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0005] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0006] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0007] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0008] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0009] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x000A] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x000B] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x000C] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x000D] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x000E] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x000F] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0010] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0011] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0012] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0013] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0014] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0015] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0016] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0017] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0018] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0019] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x001A] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x001B] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x001C] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x001D] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x001E] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x001F] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0020] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0021] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0022] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0023] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0024] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0025] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0026] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0027] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0028] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0029] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x002A] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x002B] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x002C] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x002D] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x002E] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x002F] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0030] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0031] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0032] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0033] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0034] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0035] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0036] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0037] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0038] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0039] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x003A] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x003B] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x003C] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x003D] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x003E] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x003F] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0040] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0041] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0042] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0043] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0044] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0045] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0046] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0047] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0048] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0049] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x004A] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x004B] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x004C] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x004D] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x004E] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x004F] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0050] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0051] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0052] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0053] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0054] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0055] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0056] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0057] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0058] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0059] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x005A] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x005B] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x005C] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x005D] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x005E] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x005F] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0060] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0061] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0062] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0063] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0064] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0065] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0066] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0067] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0068] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0069] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x006A] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x006B] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x006C] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x006D] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x006E] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x006F] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0070] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0071] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0072] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0073] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0074] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0075] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0076] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0077] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0078] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0079] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x007A] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x007B] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x007C] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x007D] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x007E] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x007F] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0080] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0081] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0082] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0083] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0084] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0085] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0086] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0087] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0088] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0089] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x008A] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x008B] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x008C] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x008D] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x008E] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x008F] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0090] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0091] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0092] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0093] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0094] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0095] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0096] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0097] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0098] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x0099] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x009A] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x009B] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x009C] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x009D] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x009E] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x009F] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00A0] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00A1] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00A2] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00A3] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00A4] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00A5] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00A6] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00A7] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00A8] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00A9] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00AA] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00AB] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00AC] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00AD] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00AE] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00AF] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00B0] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00B1] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00B2] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00B3] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00B4] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00B5] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00B6] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00B7] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00B8] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00B9] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00BA] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00BB] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00BC] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00BD] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00BE] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00BF] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00C0] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00C1] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00C2] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00C3] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00C4] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00C5] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00C6] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00C7] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00C8] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00C9] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00CA] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00CB] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00CC] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00CD] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00CE] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00CF] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00D0] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00D1] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00D2] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00D3] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00D4] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00D5] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00D6] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00D7] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00D8] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00D9] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00DA] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00DB] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00DC] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00DD] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00DE] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00DF] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00E0] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00E1] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00E2] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00E3] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00E4] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00E5] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00E6] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00E7] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00E8] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00E9] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00EA] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00EB] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00EC] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00ED] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00EE] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00EF] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00F0] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00F1] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00F2] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00F3] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00F4] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00F5] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00F6] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00F7] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00F8] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00F9] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00FA] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00FB] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00FC] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00FD] */
            memory[offset++] = note[count++]; /* memory[PAGE_OFFSET | 0x00FE] */
            memory[offset] = note[count];     /* memory[PAGE_OFFSET | 0x00FF] */
            --i; /* one page less left to write */
            if (node == 0x0001) /* last page of note, judging by node index */
            {
                if (i == 0) /* last page of note, judging by file size */
                {
                    stream = fopen("cont_ram.bin", "wb");
                    fwrite(memory, sizeof(unsigned char), 0x8000, stream);
                    fclose(stream);
                    return;
                }
                else
                {
                    printf("Wrong file size.\n");
                    return;
                }
            }
            ++count;
            offset = node;
        } while (i == i);
    }
    else
    {
        printf("Bad file size.\n");
        return;
    }
}

void note_rename(char *string)
{
    FILE *stream;
    unsigned char code_page_reverse[128] = { /* Convert ASCII to Nintendo. */
        000, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
         64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
         15, 52, 53, 54, 64, 64, 64, 55, 64, 64, 56, 57, 58, 59, 60, 61,
         16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 62, 64, 64, 63, 64, 64,
         65, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
         41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64,
         64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
         41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64 };
    register char note;
    register unsigned int offset;
    printf("Specify the game note (0:F):  ");
    note = digits[getchar()];
    offset = 0x0310 + (note << 5);
    memory[offset++] = code_page_reverse[string[ 0]];
    memory[offset++] = code_page_reverse[string[ 1]];
    memory[offset++] = code_page_reverse[string[ 2]];
    memory[offset++] = code_page_reverse[string[ 3]];
    memory[offset++] = code_page_reverse[string[ 4]];
    memory[offset++] = code_page_reverse[string[ 5]];
    memory[offset++] = code_page_reverse[string[ 6]];
    memory[offset++] = code_page_reverse[string[ 7]];
    memory[offset++] = code_page_reverse[string[ 8]];
    memory[offset++] = code_page_reverse[string[ 9]];
    memory[offset++] = code_page_reverse[string[10]];
    memory[offset++] = code_page_reverse[string[11]];
    memory[offset++] = code_page_reverse[string[12]];
    memory[offset++] = code_page_reverse[string[13]];
    memory[offset++] = code_page_reverse[string[14]];
    memory[offset]   = code_page_reverse[string[15]];
    stream = fopen("cont_ram.bin", "wb");
    fwrite(memory, sizeof(unsigned char), 0x8000, stream);
    fclose(stream);
    return;
}

void page_export(char *integer)
{
    FILE *stream;
    char *filename = "page_00.bin";
    unsigned char page[256];
    register unsigned int offset;

    offset  = digits[integer[0]];
    offset  = offset << 4;
    offset |= digits[integer[1]];
    if (offset > 127)
    { /* if MOV(offset, -1) then offset == 0xFFFF */
        printf("Invalid page.\n");
        return;
    }
    filename[5] = integer[0];
    filename[6] = integer[1];

    offset = offset << 8; /* 0x00?? * 0x0100 == 0x??00 */
    page[0x00] = memory[offset++];
    page[0x01] = memory[offset++];
    page[0x02] = memory[offset++];
    page[0x03] = memory[offset++];
    page[0x04] = memory[offset++];
    page[0x05] = memory[offset++];
    page[0x06] = memory[offset++];
    page[0x07] = memory[offset++];
    page[0x08] = memory[offset++];
    page[0x09] = memory[offset++];
    page[0x0A] = memory[offset++];
    page[0x0B] = memory[offset++];
    page[0x0C] = memory[offset++];
    page[0x0D] = memory[offset++];
    page[0x0E] = memory[offset++];
    page[0x0F] = memory[offset++];
    page[0x10] = memory[offset++];
    page[0x11] = memory[offset++];
    page[0x12] = memory[offset++];
    page[0x13] = memory[offset++];
    page[0x14] = memory[offset++];
    page[0x15] = memory[offset++];
    page[0x16] = memory[offset++];
    page[0x17] = memory[offset++];
    page[0x18] = memory[offset++];
    page[0x19] = memory[offset++];
    page[0x1A] = memory[offset++];
    page[0x1B] = memory[offset++];
    page[0x1C] = memory[offset++];
    page[0x1D] = memory[offset++];
    page[0x1E] = memory[offset++];
    page[0x1F] = memory[offset++];
    page[0x20] = memory[offset++];
    page[0x21] = memory[offset++];
    page[0x22] = memory[offset++];
    page[0x23] = memory[offset++];
    page[0x24] = memory[offset++];
    page[0x25] = memory[offset++];
    page[0x26] = memory[offset++];
    page[0x27] = memory[offset++];
    page[0x28] = memory[offset++];
    page[0x29] = memory[offset++];
    page[0x2A] = memory[offset++];
    page[0x2B] = memory[offset++];
    page[0x2C] = memory[offset++];
    page[0x2D] = memory[offset++];
    page[0x2E] = memory[offset++];
    page[0x2F] = memory[offset++];
    page[0x30] = memory[offset++];
    page[0x31] = memory[offset++];
    page[0x32] = memory[offset++];
    page[0x33] = memory[offset++];
    page[0x34] = memory[offset++];
    page[0x35] = memory[offset++];
    page[0x36] = memory[offset++];
    page[0x37] = memory[offset++];
    page[0x38] = memory[offset++];
    page[0x39] = memory[offset++];
    page[0x3A] = memory[offset++];
    page[0x3B] = memory[offset++];
    page[0x3C] = memory[offset++];
    page[0x3D] = memory[offset++];
    page[0x3E] = memory[offset++];
    page[0x3F] = memory[offset++];
    page[0x40] = memory[offset++];
    page[0x41] = memory[offset++];
    page[0x42] = memory[offset++];
    page[0x43] = memory[offset++];
    page[0x44] = memory[offset++];
    page[0x45] = memory[offset++];
    page[0x46] = memory[offset++];
    page[0x47] = memory[offset++];
    page[0x48] = memory[offset++];
    page[0x49] = memory[offset++];
    page[0x4A] = memory[offset++];
    page[0x4B] = memory[offset++];
    page[0x4C] = memory[offset++];
    page[0x4D] = memory[offset++];
    page[0x4E] = memory[offset++];
    page[0x4F] = memory[offset++];
    page[0x50] = memory[offset++];
    page[0x51] = memory[offset++];
    page[0x52] = memory[offset++];
    page[0x53] = memory[offset++];
    page[0x54] = memory[offset++];
    page[0x55] = memory[offset++];
    page[0x56] = memory[offset++];
    page[0x57] = memory[offset++];
    page[0x58] = memory[offset++];
    page[0x59] = memory[offset++];
    page[0x5A] = memory[offset++];
    page[0x5B] = memory[offset++];
    page[0x5C] = memory[offset++];
    page[0x5D] = memory[offset++];
    page[0x5E] = memory[offset++];
    page[0x5F] = memory[offset++];
    page[0x60] = memory[offset++];
    page[0x61] = memory[offset++];
    page[0x62] = memory[offset++];
    page[0x63] = memory[offset++];
    page[0x64] = memory[offset++];
    page[0x65] = memory[offset++];
    page[0x66] = memory[offset++];
    page[0x67] = memory[offset++];
    page[0x68] = memory[offset++];
    page[0x69] = memory[offset++];
    page[0x6A] = memory[offset++];
    page[0x6B] = memory[offset++];
    page[0x6C] = memory[offset++];
    page[0x6D] = memory[offset++];
    page[0x6E] = memory[offset++];
    page[0x6F] = memory[offset++];
    page[0x70] = memory[offset++];
    page[0x71] = memory[offset++];
    page[0x72] = memory[offset++];
    page[0x73] = memory[offset++];
    page[0x74] = memory[offset++];
    page[0x75] = memory[offset++];
    page[0x76] = memory[offset++];
    page[0x77] = memory[offset++];
    page[0x78] = memory[offset++];
    page[0x79] = memory[offset++];
    page[0x7A] = memory[offset++];
    page[0x7B] = memory[offset++];
    page[0x7C] = memory[offset++];
    page[0x7D] = memory[offset++];
    page[0x7E] = memory[offset++];
    page[0x7F] = memory[offset++];
    page[0x80] = memory[offset++];
    page[0x81] = memory[offset++];
    page[0x82] = memory[offset++];
    page[0x83] = memory[offset++];
    page[0x84] = memory[offset++];
    page[0x85] = memory[offset++];
    page[0x86] = memory[offset++];
    page[0x87] = memory[offset++];
    page[0x88] = memory[offset++];
    page[0x89] = memory[offset++];
    page[0x8A] = memory[offset++];
    page[0x8B] = memory[offset++];
    page[0x8C] = memory[offset++];
    page[0x8D] = memory[offset++];
    page[0x8E] = memory[offset++];
    page[0x8F] = memory[offset++];
    page[0x90] = memory[offset++];
    page[0x91] = memory[offset++];
    page[0x92] = memory[offset++];
    page[0x93] = memory[offset++];
    page[0x94] = memory[offset++];
    page[0x95] = memory[offset++];
    page[0x96] = memory[offset++];
    page[0x97] = memory[offset++];
    page[0x98] = memory[offset++];
    page[0x99] = memory[offset++];
    page[0x9A] = memory[offset++];
    page[0x9B] = memory[offset++];
    page[0x9C] = memory[offset++];
    page[0x9D] = memory[offset++];
    page[0x9E] = memory[offset++];
    page[0x9F] = memory[offset++];
    page[0xA0] = memory[offset++];
    page[0xA1] = memory[offset++];
    page[0xA2] = memory[offset++];
    page[0xA3] = memory[offset++];
    page[0xA4] = memory[offset++];
    page[0xA5] = memory[offset++];
    page[0xA6] = memory[offset++];
    page[0xA7] = memory[offset++];
    page[0xA8] = memory[offset++];
    page[0xA9] = memory[offset++];
    page[0xAA] = memory[offset++];
    page[0xAB] = memory[offset++];
    page[0xAC] = memory[offset++];
    page[0xAD] = memory[offset++];
    page[0xAE] = memory[offset++];
    page[0xAF] = memory[offset++];
    page[0xB0] = memory[offset++];
    page[0xB1] = memory[offset++];
    page[0xB2] = memory[offset++];
    page[0xB3] = memory[offset++];
    page[0xB4] = memory[offset++];
    page[0xB5] = memory[offset++];
    page[0xB6] = memory[offset++];
    page[0xB7] = memory[offset++];
    page[0xB8] = memory[offset++];
    page[0xB9] = memory[offset++];
    page[0xBA] = memory[offset++];
    page[0xBB] = memory[offset++];
    page[0xBC] = memory[offset++];
    page[0xBD] = memory[offset++];
    page[0xBE] = memory[offset++];
    page[0xBF] = memory[offset++];
    page[0xC0] = memory[offset++];
    page[0xC1] = memory[offset++];
    page[0xC2] = memory[offset++];
    page[0xC3] = memory[offset++];
    page[0xC4] = memory[offset++];
    page[0xC5] = memory[offset++];
    page[0xC6] = memory[offset++];
    page[0xC7] = memory[offset++];
    page[0xC8] = memory[offset++];
    page[0xC9] = memory[offset++];
    page[0xCA] = memory[offset++];
    page[0xCB] = memory[offset++];
    page[0xCC] = memory[offset++];
    page[0xCD] = memory[offset++];
    page[0xCE] = memory[offset++];
    page[0xCF] = memory[offset++];
    page[0xD0] = memory[offset++];
    page[0xD1] = memory[offset++];
    page[0xD2] = memory[offset++];
    page[0xD3] = memory[offset++];
    page[0xD4] = memory[offset++];
    page[0xD5] = memory[offset++];
    page[0xD6] = memory[offset++];
    page[0xD7] = memory[offset++];
    page[0xD8] = memory[offset++];
    page[0xD9] = memory[offset++];
    page[0xDA] = memory[offset++];
    page[0xDB] = memory[offset++];
    page[0xDC] = memory[offset++];
    page[0xDD] = memory[offset++];
    page[0xDE] = memory[offset++];
    page[0xDF] = memory[offset++];
    page[0xE0] = memory[offset++];
    page[0xE1] = memory[offset++];
    page[0xE2] = memory[offset++];
    page[0xE3] = memory[offset++];
    page[0xE4] = memory[offset++];
    page[0xE5] = memory[offset++];
    page[0xE6] = memory[offset++];
    page[0xE7] = memory[offset++];
    page[0xE8] = memory[offset++];
    page[0xE9] = memory[offset++];
    page[0xEA] = memory[offset++];
    page[0xEB] = memory[offset++];
    page[0xEC] = memory[offset++];
    page[0xED] = memory[offset++];
    page[0xEE] = memory[offset++];
    page[0xEF] = memory[offset++];
    page[0xF0] = memory[offset++];
    page[0xF1] = memory[offset++];
    page[0xF2] = memory[offset++];
    page[0xF3] = memory[offset++];
    page[0xF4] = memory[offset++];
    page[0xF5] = memory[offset++];
    page[0xF6] = memory[offset++];
    page[0xF7] = memory[offset++];
    page[0xF8] = memory[offset++];
    page[0xF9] = memory[offset++];
    page[0xFA] = memory[offset++];
    page[0xFB] = memory[offset++];
    page[0xFC] = memory[offset++];
    page[0xFD] = memory[offset++];
    page[0xFE] = memory[offset++];
    page[0xFF] = memory[offset];
    stream = fopen(filename, "wb");
    fwrite(page, sizeof(unsigned char), 256, stream);
    fclose(stream);
    return;
}
